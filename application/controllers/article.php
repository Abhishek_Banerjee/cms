<?php
class Article extends MY_Controller {

    public function __construct() {
        parent::__construct();
        //Load models
        $this->load->model('article_m');
        $this->load->library('session');
        // Fetch navigation
        $this->data['loggedin'] = $this->article_m->loggedin();
        $this->data['nick'] = $this->data['loggedin'] == TRUE ? $this->session->userdata('nick') : 'Sign in';
        $this->data['meta_title'] = config_item('site_name');
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('comments_m');
    }
    
    public function a() {
        $var = $this->article_m->get_recent_();
        dump($var);
    }
    
    public function index($slug) {
//        else {
            // Fetch the article
            $this->data['recent_blog'] = $this->article_m->get_recent('articles');
            $this->data['recent_tut'] = $this->article_m->get_recent('tutorials');
//            dump($this->data['recent_blog']);
            $this->article_m->set_published();
            $this->data['article'] = $this->article_m->get_article($slug);
            // Return 404 if not found
            count($this->data['article']) || show_404(uri_string());
            
            // Fetch blog author's info
            $this->data['author'] = $this->article_m->get_author($this->data['article']->user_id);
            
            
            
            $str = $this->article_m->get_ace_tags($this->data['article']->id, 'blog');
        
            if(isset($str->tag) && $str->tag != NULL) {

                $atags = explode('#', $str->tag);
                $this->data['ace_tags'] = '<script src="http://ajaxorg.github.io/ace-builds/src-noconflict/ace.js" '
                        . 'type="text/javascript" charset="utf-8"></script><script>';
                //var_dump($str->tags);
                foreach($atags as $atag) {
                    $l = explode('-', $atag);
                    $this->data['ace_tags'] .= 'var j'.$l[0].' = ace.edit("'.$atag.'"); cnf(j'.$l[0].', "'.$l[0].'");';
                }
                $this->data['ace_tags'] .= 'function cnf(v,lan) {
                                                v.setTheme("ace/theme/textmate");
                                                v.getSession().setMode("ace/mode/"+lan);
                                                v.setReadOnly(true);
                                                v.setOptions({maxLines: 100});
                                            }</script>';
            }
//            $this->data['vote'] = -1;
            //Stars
            if($this->article_m->loggedin()) {
                $s = $this->article_m->get_stars_by_id($this->session->userdata('id'), 'blog', $id);
//                $v = $this->article_m->get_votes_by_id($this->session->userdata('id'), 'blog', $id);
                if (count($s) !== 0) {
                    $this->data['img'] = 'star.png';
                }
                else {
                    $this->data['img'] = 'star2.png';
                }
//                if (count($v) !== 0) {
//                    $this->data['vote'] = $v->upvote;
//                }
            }
            else {
                $this->data['img'] = 'star2.png';
//                $this->data['voted'] = FALSE;
            }
            add_meta_title($this->data['article']->title);
            $this->data['subview'] = 'article';
            $this->load->helper('date');
            $this->data['modified'] = $this->getTime($this->data['article']->modified);
//        }
            $this->load->view('_layout_main', $this->data);
            
            //$this->load->view('templates/d',$data);
    }
    
    //Not xss Filtered
    public function star_click($page,$id) {
        if($this->input->is_ajax_request() && $this->article_m->loggedin()) {
            $b = $this->input->post('add');
//            echo $id.$page;
            $count = $this->article_m->get_total_stars($id, 'articles');
//            var_dump($count);
//            die();
            if(count($count) > 0) {
                $id = $this->article_m->save_stars($b, $id, $page, $this->session->userdata('id'), $count->total_stars);
                echo $id;
                die();
            }
            
        }
        echo 'error';
    }
    
    public function vote_click($page,$id) {
        if($this->input->is_ajax_request() && $this->article_m->loggedin()) {
            $b = $this->input->post('upvote');
            if($b == 0 && $this->get_points() < 50) {
                return 'p';
            }
//            echo $id.$page;
            $count = $this->article_m->get_total_votes($id, 'articles');
//            var_dump($count);
//            die();
            if(count($count) > 0) {
                $id = $this->article_m->save_votes($b, $id, $page, $this->session->userdata('id'), $count->votes);
//                var_dump($id);
                echo $id;
                die();
            }
            
        }
        echo 'error';
    }
    
    public function get_by_tag($tag) {
        
        // Count all articles
        $this->article_m->set_published();
        $this->data['article'] = NULL;
        $da = $this->article_m->get_count('%#'.$tag.'#%');
        $count = (int) $da->c;
        $perPage = 4;
        
        //Set Pagination
        if ($count > $perPage) {
            $this->load->library('pagination');
            
            $config['base_url'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'. $this->uri->segment(3));
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 4;
            
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(4);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        //Fetch articles
//        $this->article_m->set_published();
        $this->db->limit($perPage, $offset);
        $this->db->where('`tags` like \'%#'.$tag.'#%\'');
        $this->data['articles'] = $this->article_m->get();
        $this->load->helper('date');
        for($i = 0; $i < count($this->data['articles']); $i++) {
            $this->data['articles'][$i]->modified = $this->getTime($this->data['articles'][$i]->modified);
        }
        $this->data['recent_tut'] = $this->article_m->get_recent('tutorials');
        $this->data['recent_blog'] = $this->article_m->get_recent('articles');
        $this->data['subview'] = 'blog';
        $this->load->view('_layout_main', $this->data);
        
    }
    
}