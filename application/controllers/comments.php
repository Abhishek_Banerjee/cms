<?php
class Comments extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('page_m');
        $this->load->model('frontend_user_m');
        $this->load->library('session');
        // Fetch navigation
        $this->data['loggedin'] = $this->frontend_user_m->loggedin();
        $this->data['nick'] = $this->data['loggedin'] == TRUE ? $this->session->userdata('nick') : 'Sign in';
        $this->data['meta_title'] = config_item('site_name');
        
        $this->load->model('comments_m');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        
    }
    
    public function index($page = NULL, $id = NULL, $slug=NULL) {
        
        if($this->data['loggedin'] == TRUE) {
            
            $this->data['comments'] = $this->comments_m->get_new();
            $rules = $this->comments_m->rules;
            $this->form_validation->set_rules($rules);
            
            if ($this->form_validation->run() == TRUE) {
                $this->load->helper('security');
                $comments['comment'] = xss_clean($this->input->post('comment'));
                $comments['nick'] = xss_clean($this->input->post('nick'));
                $comments['reply'] = xss_clean($this->input->post('reply'));
                if($this->input->post('reply') == 1) {
                    $cid = xss_clean($this->input->post('comment_id'));
                    
                }
                else {
                    $cid = $this->comments_m->get_id();
                    $cid = isset($cid->id) ? $cid->id:1;
                    
                }
                $comments['comment_id'] = $cid;
                $comments['page'] = xss_clean($page);
                $comments['slug'] = xss_clean($slug);
                $comments['user_id'] = $this->session->userdata('id');
                $this->load->helper('date');
                $comments['pubdate'] = time();
                $this->comments_m->save($comments);
            }
            else {
                echo validation_errors();
            }
        }
        else {
        if($this->input->post('email')) {
                $this->data['comments'] = $this->comments_m->get_new();
                
                // Set form
                $rules = $this->frontend_user_m->rules;
                $this->form_validation->set_rules($rules);
                // Process form
                if ($this->form_validation->run() == TRUE) {
                    
                    // We can login and redirect
                    $this->frontend_user_m->login();
                    $nick1 = $this->session->userdata('nick');
                    $dat['nick'] = $nick1;
                    $dat['url1'] = site_url('user/'.$nick1.'/account');
                    $dat['url2'] = site_url('logout');
                    if (isset($nick1)) {
                        if($this->input->post('nick') == 'Anonymous') {
                            $nick1 = 'Anonymous';
                        }
                        $rules = $this->comments_m->rules;
                        $this->form_validation->set_rules($rules);
                        if ($this->form_validation->run() == TRUE) {
                            $this->load->helper('security');
                            $comments['comment'] = xss_clean($this->input->post('comment'));
                            $comments['reply'] = xss_clean($this->input->post('reply'));
                            if($this->input->post('reply') == 1) {
                                $cid = xss_clean($this->input->post('comment_id'));

                            }
                            else {
                                $cid = $this->comments_m->get_id();
                                $cid = isset($cid->id) ? $cid->id:1;

                            }
                            
                            $comments['comment_id'] = $cid;
                            $comments['nick'] = $nick1;
                            $comments['page'] = xss_clean($page);
                            $comments['slug'] = xss_clean($slug);
                            $comments['user_id'] = $this->session->userdata('id');
                            $this->load->helper('date');
                            $comments['pubdate'] = time();
                            $this->comments_m->save($comments);
                            
                        }
                        else {
                            echo validation_errors();
                        }
                    }
                    else {
                        echo 'password didnt match';
                   }
                }
                
            }
        }
        redirect(site_url('/'.$page.'/'.$id.'/'.$slug));
    }
    
    public function get_comments($page, $slug) {
        if(!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        else {
            if($slug) {
                $this->load->helper('security');
                $slug = xss_clean($slug);
                $page = xss_clean($page);
                $result = $this->comments_m->get_comments($page, $slug);
                $this->load->helper('date');
                for($i = 0; $i < count($result); $i++) {
                    $result[$i]['pubdate'] = $this->getTime($result[$i]['pubdate']);
                }
                echo json_encode($result);
            }
        }
    }
    
}