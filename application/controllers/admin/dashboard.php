<?php
class Dashboard extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->data['meta_title'] = 'CMS';
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('user_m');
        
        $exception_urls = array('admin/user/login', 'admin/user/logout');
        
        if (in_array(uri_string(), $exception_urls) == FALSE) {
            if($this->user_m->loggedin() == FALSE) {
                redirect('admin/user/login');
            }
            
        }
    }
    
    function index() {
        // Fetch recently modified articles
    	$this->load->model('article_m');
    	$this->db->order_by('modified desc');
    	$this->db->limit(5);
    	$this->data['recent_articles'] = $this->article_m->get();
        
        $this->data['subview'] = 'admin/dashboard/index';
        $this->load->view('admin/_layout_main', $this->data);
        
    }
    
    function modal() {
        $this->load->view('admin/_layout_modal', $this->data);
    }
    
}
