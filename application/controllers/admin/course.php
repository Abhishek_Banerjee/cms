<?php

class  Course extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->data['meta_title'] = 'CMS';
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('user_m');
        
        $exception_urls = array('admin/user/login', 'admin/user/logout');
        
        if (in_array(uri_string(), $exception_urls) == FALSE) {
            if($this->user_m->loggedin() == FALSE) {
                redirect('admin/user/login');
            }
            
        }
        $this->load->model('course_m');
    }
    
    public function index() {
        $this->data['courses'] = $this->course_m->get();
        $this->data['subview'] = 'admin/course/index';
        $this->load->view('admin/_layout_main', $this->data);
    }
    
    public function edit($id = NULL) {
        
        if($id) {
            $this->data['course'] = $this->course_m->get($id);
            count($this->data['course']) || $this->data['errors'] = 'course could not be found.';
        }
        else {
            $this->data['course'] = $this->course_m->get_new();
        }
        
        
        $rules = $this->course_m->rules;
        $this->form_validation->set_rules($rules);
        
        if ($this->form_validation->run() == TRUE) {
            $data = $this->course_m->array_from_post(array('title', 'slug', 'image', 
                'syllabus', 'summary', 'instructor', 'time', 'tags' , 'links', 'pubdate'));
            $this->course_m->save($data, $id);
            redirect('admin/course');
        }
        $this->data['subview'] = 'admin/course/edit';
        $this->load->view('admin/_layout_main', $this->data);
    }
    
    public function delete($id) {
        $this->course_m->delete($id);
        redirect('admin/course');
    }
}
