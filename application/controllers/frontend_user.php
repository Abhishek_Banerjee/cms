<?php

class Frontend_user extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('frontend_user_m');
        $this->load->library('session');
        // Fetch navigation
        $this->data['loggedin'] = $this->frontend_user_m->loggedin();
        $this->data['nick'] = $this->data['loggedin'] == TRUE ? $this->session->userdata('nick') : 'signin';
        $this->data['meta_title'] = config_item('site_name');
        
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('', '');
    }
    
    public function index() {
        
        // Redirect a user if he's already logged in
        $this->frontend_user_m->loggedin() == FALSE || redirect(site_url());
        
        //Google sign in
        
        require_once (BASEPATH.'../g/src/Google_Client.php');
        require_once (BASEPATH.'../g/src/contrib/Google_Oauth2Service.php');
        session_start();
        $client = new Google_Client();
        $client->setApplicationName("Tutepoint.org");
        // Visit https://code.google.com/apis/console?api=plus to generate your
        // oauth2_client_id, oauth2_client_secret, and to register your oauth2_redirect_uri.
         $client->setClientId('970290268215-btgr1ouunrus82tic0ajkij8ba5aviid.apps.googleusercontent.com');
         $client->setClientSecret('hnISpU8V11nECGlAs8YAsncm');
         $client->setRedirectUri('http://localhost/cms/public_html/index.php/login');
        // $client->setDeveloperKey('insert_your_developer_key');
        $oauth2 = new Google_Oauth2Service($client);

        if (isset($_GET['code'])) {
            $client->authenticate($_GET['code']);
            $_SESSION['token'] = $client->getAccessToken();
            $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
            header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
            return;
        }
        
        if (isset($_SESSION['token'])) {
            $client->setAccessToken($_SESSION['token']);
        }

        if ($client->getAccessToken()) {
            $this->data['useri'] = $oauth2->userinfo->get();
//            dump($this->data['useri']);
            $v = $this->frontend_user_m->getDetails($this->data['useri']['email']);
            if($v !== NULL) {
                $udata = array('name' => $v->name,
                                'email' => $this->data['useri']['email'],
                                'id' => $v->id,
                                'nick' => $v->nick,
                                'loggedin' => TRUE,
                                'type' => 1,
                                'bio' => $v->bio,
                                'image' => $this->data['useri']['picture']
                            );
            }
            else {
                $name = $this->create_valid_name($this->data['useri']['name'], $this->data['useri']['email']);
                $nick = $this->create_nick($name);
                $password = $this->create_password();
//                TODO: Send the password as email
                $udata = array('name' => $name,
                                'email' => $this->data['useri']['email'],
                                'id' => '',
                                'nick' => $nick,
                                'loggedin' => TRUE,
                                'type' => 1,
                                'bio' => NULL,
                                'image' => $this->data['useri']['picture']
                            );
                $data1 = array(
                                'name' => $name,
                                'nick' => $nick,
                                'image' => $this->data['useri']['picture'],
                                'email' => $this->data['useri']['email'],
                                'password' => $this->frontend_user_m->hash($password)
                                );
                
                $this->frontend_user_m->save($data1, NULL);
                
            }
            $this->frontend_user_m->setUserData($udata);
            if ($this->frontend_user_m->loggedin() == TRUE) {
                $this->data['subview'] = 'login';
                $this->load->view('_layout_main', $this->data);
                unset($_SESSION['token']);
                $client->revokeToken();
                redirect(site_url());
//                session_destroy();
            }
        } else {
            $this->data['authUrl'] = $client->createAuthUrl();
        }
        
        if (isset($_REQUEST['logout'])) {
            unset($_SESSION['token']);
            $client->revokeToken();
        }
        //
        $this->data['subview'] = 'login';
        $this->load->view('_layout_main', $this->data);
    }
    
    private function create_valid_name($name, $email) {
        if($name == 'me' || $name == 'anonymous'||$name == '') {
            return $email;
        }
        return $name;
    }
    
    private function create_password() {
        return substr(sha1(time()), 0, 9);
    }
    
    private function create_nick($name) {
        $name = str_replace(' ', '_', $name);
        while(TRUE) {
            $g = $this->frontend_user_m->get_nick($name);
            if($g !== NULL) {
                $name .= rand(100000, 99999999);
            }
            else {
                break;
            }
        }
        return $name;
    }


    public function edit($id = NULL) {
        // Fetch a user or set a new one
        if($id) {
            $this->data['frontend_user'] = $this->frontend_user_m->get($id);
            count($this->data['frontend_user']) || $this->data['errors'] = 'user could not be found.';
        }
        else {
            $this->data['frontend_user'] = $this->frontend_user_m->get_new();
        }
        $rules = $this->frontend_user_m->rules_user;
        $id || $rules['password']['rules'] .= '|required';
        $this->form_validation->set_rules($rules);
        // Process the form
        if ($this->form_validation->run() == TRUE && $this->_validate_nick()) {
            
            $data = $this->frontend_user_m->array_from_post(array('name', 'nick', 'email', 'password'));
            $data['password'] = $this->frontend_user_m->hash($data['password']);
            
            $this->frontend_user_m->save($data, $id);
            $this->frontend_user_m->login();
            echo 'success';
        }
        else {
            echo validation_errors();
        }
    }

    public function _unique_email() {
        
        // Do NOT validate if email already exists
        // UNLESS it's the email for the current user
        $this->db->where('email', $this->input->post('email'));
        $frontend_user = $this->frontend_user_m->get();
        if (count($frontend_user)) {
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }
        return TRUE;
    }
    
    public function delete($id) {
        $this->frontend_user_m->delete($id);
        redirect(site_url());
    }
    
    public function login() {
        if ($this->frontend_user_m->loggedin() == TRUE) {
            redirect(site_url());
        }
        // Set form
        $rules = $this->frontend_user_m->rules;
        $this->form_validation->set_rules($rules);
        // Process form
        if ($this->form_validation->run() == TRUE) {
            // We can login and redirect
            if(!$this->frontend_user_m->login()) {
                echo 'Username password does not match.';
            }
            if ($this->frontend_user_m->loggedin() == TRUE) {
                echo "success";
            }
        }
        else {
            echo 'Invalid Email';
        }
    }
    
    public function logout() {
        $this->frontend_user_m->logout();
        redirect(site_url());
    }
    
    public function forgot_password() {
        
        $this->load->helper('email');
        $this->load->helper('security');
        if($this->input->post('email')) {
            if(!valid_email($this->input->post('email'))) {
                $this->data['link'] = FALSE;
                
                $this->data['subview'] = 'forgot_password';
                $this->load->view('_layout_main', $this->data);
                
            }
            else {
////                $this->load->library('email');
//                $this->email->set_newline("\r\n");
//                
//
//                $this->email->from('ooabhi1992oo@gmail.com', 'CMS');
//                $this->email->to($email); 
//
//                $this->email->subject('CMS Password Reset Link');
//                $this->email->message('Password Reset Link');	
//
//                $this->email->send();
//                $this->data['error'] = $this->email->print_debugger();
//                send_email($email, 'CMS Password Reset Link', 'Password Reset Link');
                $this->data['link'] = TRUE;
                $email = xss_clean($this->input->post('email'));
                if(mail($email, 'CMS Password Reset Link', 'Password Reset Link')) {$this->data['ddg'] = "False";}
                else {
        }
            }
        }
        else {
            $this->data['link'] = FALSE;
        }
        
        $this->data['subview'] = 'forgot_password';
        $this->load->view('_layout_main', $this->data);
    }
    
    public function password_reset() {
        $this->data['subview'] = 'forgot_password';
        $this->data['link'] = TRUE;
        $this->load->view('_layout_main', $this->data);
    }
    
    public function account($nick) {
        $this->load->helper('security');
        $nick1 = xss_clean($nick);
        $data=$this->frontend_user_m->get_by('nick = \''. $nick1.'\'', TRUE);
        if($data == NULL) {
            show_404();
        }
        $nick = $data->nick;
        $name = $data->name;
        $bio = $data->bio;
        $image = $data->image;
        $this->data['useri'] = array('image'=>$image, 'name'=> $name,'nick'=> $nick, 'bio'=>$bio);
        $this->data['editLinks'] = FALSE;
        if($this->frontend_user_m->loggedin() and $this->session->userdata['nick'] == $nick) {
            
            $this->data['editLinks'] = TRUE;
        }
        $this->data['subview'] = 'account';
        $this->load->view('_layout_main', $this->data);
    }
    
    public function _validate_nick() {
        if($this->input->post('nick') == 'me' || $this->input->post('nick') == 'anonymous') {
            $this->form_validation->set_message('_validate_nick', '%s should be unique');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    public function account_edit() {
        if($this->frontend_user_m->loggedin()) {
        if($this->input->post('nick')) {
            $this->load->helper('security');
            
            $rules = array('name' => array('field' => 'name',
                                             'label' => 'Name',
                                             'rules' => 'trim|required|xss_clean'),
                            'nick' => array('field' => 'nick',
                                             'label' => 'Nick',
                                             'rules' => 'trim|required|xss_clean|callback__validate_nick'), 
                            'bio' => array('field' => 'bio',
                                             'label' => 'Bio',
                                             'rules' => 'trim|xss_clean|max_length[50]'));
            if($this->input->post('nick') !== $this->session->userdata('nick')) {
                $rules['nick']['rules'] .= '|is_unique[frontend_users.nick]';
            }
            $this->form_validation->set_rules($rules);
            // Process the form
            if ($this->form_validation->run() == TRUE) {
                
                $data = $this->frontend_user_m->array_from_post(array('name', 'nick', 'bio'));
                $id = $this->session->userdata('id');
                $n = $this->frontend_user_m->get_by(array('nick' =>  $this->input->post('nick')), TRUE);
                
                if(count($n) == 0 || $n->id == $id) {
                    $this->frontend_user_m->edit_details($data);
                    redirect('profile/'.$this->session->userdata('nick'));
                }
                else {
                    
                    $this->data['subview'] = 'account_edit';
                    $this->load->view('_layout_main', $this->data);
                    return;
                }
                
            }
            else {
                    $this->udata();
                    $this->data['subview'] = 'account_edit';
                    $this->load->view('_layout_main', $this->data);
                    return;
                }
        }
        else {
            $this->udata();
            $this->data['subview'] = 'account_edit';
            $this->load->view('_layout_main', $this->data);
            return;
            
        }
        }
        else {
            redirect(site_url('login'));
        }
    }
    
    function save_image() {
        if($this->input->is_ajax_request()) {
            if($this->frontend_user_m->loggedin()) {
                $data=$this->frontend_user_m->get_by('nick = \''. $this->session->userdata('nick').'\'', TRUE);
                $this->load->helper('security');
                $data->image = xss_clean($this->input->post('image'));
                $this->frontend_user_m->save($data, $data->id);
                echo 'success';
            }
            else {
                echo 'fail';
            }
        }
        else {
            echo 'fail';
        }
    }
    
    function udata() {
        $data=$this->frontend_user_m->get_by('nick = \''. $this->session->userdata('nick').'\'', TRUE);
        if($data == NULL) {
            show_404();
        }
        $nick = $data->nick;
        $name = $data->name;
        $bio = $data->bio;
        $image = $data->image;
        $this->data['useri'] = array('image'=>$image, 'name'=> $name,'nick'=> $nick, 'bio'=>$bio);
    }
}
