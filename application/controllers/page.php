<?php

class Page extends MY_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('page_m');
        $this->load->library('session');
        // Fetch navigation
        $this->data['loggedin'] = $this->page_m->loggedin();
        $this->data['nick'] = $this->data['loggedin'] == TRUE ? $this->session->userdata('nick') : 'Sign in';
        $this->data['meta_title'] = config_item('site_name');
    }
    
    public function home() {
        $this->load->view('templates/homepage', $this->data);
    }

    public function index()
    {
        
        $this->data['page'] = $this->page_m->get_by(array('slug' => (string) $this->uri->segment(1)), TRUE);
        count($this->data['page']) || show_404(current_url());
        
        add_meta_title($this->data['page']->title);
        if($this->data['page']->template == 'homepage') {
            $this->home();
            return;
        }
        $method = '_'.$this->data['page']->template;
        //echo '<br><br><br><br><br><br><br><br>'.$method;
        if(method_exists($this, $method)) {
            $this->$method();
        }
        else {
            log_message('error', 'Could not load template '.$method.' in file '.__FILE__.' at line '.__LINE__);
            show_error('Could not load template '.$method);
        }
        $this->data['subview'] = $this->data['page']->template;
        $this->load->view('_layout_main', $this->data);
        
    }
    
    
    private function _page() {
        $this->data['recent_blog'] = $this->page_m->get_recent('articles');
        $this->data['recent_tut'] = $this->page_m->get_recent('tutorials');
    }
    
    function _homepage() {
        $this->load->model('article_m');
        $this->article_m->set_published();
    	$this->db->limit(6);
    	$this->data['articles'] = $this->article_m->get();
    }
    
    private function _blog() {
        $this->load->model('article_m');
        $this->data['recent_blog'] = $this->page_m->get_recent('articles');
    	$this->data['recent_tut'] = $this->page_m->get_recent('tutorials');
        // Count all articles
        $this->article_m->set_published();
        $count = $this->db->count_all_results('articles');
        $perPage = 4;
        
        //Set Pagination
        if ($count > $perPage) {
            $this->load->library('pagination');
            
            $config['base_url'] = site_url($this->uri->segment(1));
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 2;
            
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        //Fetch articles
        $this->article_m->set_published();
        $this->db->limit($perPage, $offset);
        $this->data['articles'] = $this->article_m->get();
        $this->load->helper('date');
        for($i = 0; $i < count($this->data['articles']); $i++) {
            $this->data['articles'][$i]->modified = $this->getTime($this->data['articles'][$i]->modified);
        }
        
    }
    
    private function _tutorial() {
        $this->load->model('tutorial_m');
        // Count all tutorials
        $this->tutorial_m->set_published();
        $count = $this->db->count_all_results('tutorials');
        $perPage = 4;
        
        //Set Pagination
        if ($count > $perPage) {
            $this->load->library('pagination');
            
            $config['base_url'] = site_url($this->uri->segment(1));
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 2;
            
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        //Fetch tutorials
        $this->tutorial_m->set_published();
        $this->db->limit($perPage, $offset);
        $this->data['tutorial'] = $this->tutorial_m->get();
        $this->load->helper('date');
        for($i = 0; $i < count($this->data['tutorial']); $i++) {
            $this->data['tutorial'][$i]->modified = $this->getTime($this->data['tutorial'][$i]->modified);
        }
        $this->data['recent_tut'] = $this->page_m->get_recent('tutorials');
        $this->data['recent_blog'] = $this->page_m->get_recent('articles');
    }
    
    private function _course_archive() {
        $this->load->model('course_m');
        $this->data['recent_news'] = $this->course_m->get_recent();
    	
        // Count all courses
        $this->course_m->set_published();
        $count = $this->db->count_all_results('courses');
        $perPage = 4;
        
        //Set Pagination
        if ($count > $perPage) {
            $this->load->library('pagination');
            
            $config['base_url'] = site_url($this->uri->segment(1));
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 2;
            
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        //Fetch courses
        $this->course_m->set_published();
        $this->db->limit($perPage, $offset);
        $this->data['courses'] = $this->course_m->get();
    }
}
