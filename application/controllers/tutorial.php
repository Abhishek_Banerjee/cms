<?php
class Tutorial extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tutorial_m');
        $this->load->library('session');
        // Fetch navigation
        $this->data['loggedin'] = $this->tutorial_m->loggedin();
        $this->data['nick'] = $this->data['loggedin'] == TRUE ? $this->session->userdata('nick') : 'Sign in';
        $this->data['meta_title'] = config_item('site_name');
        
    }

    public function index($slug) {
    	// Fetch the tutorial
        $this->load->helper('form');
        $this->tutorial_m->set_published();
        $this->data['tutorial'] = $this->tutorial_m->get_by(array('slug'=>$slug), TRUE);
        // Return 404 if not found
    	count($this->data['tutorial']) || show_404(uri_string());
        $this->data['author'] = $this->tutorial_m->get_author($this->data['tutorial']->user_id);
        $this->load->helper('date');
        $this->data['modified'] = $this->getTime($this->data['tutorial']->modified);
    	$this->data['tutorial_title'] = $this->tutorial_m->get_titles_of_set($this->data['tutorial']->set);
    	
    	$this->get_next_prev($this->data['tutorial']->id, $this->data['tutorial']->category);
        $str = $this->tutorial_m->get_ace_tags($this->data['tutorial']->id, 'tutorial');
        
        if(isset($str->tag) && $str->tag != NULL) {
            
            $atags = explode('#', $str->tag);
            $this->data['ace_tags'] = '<script>window.onload = function(){ try{';
            //var_dump($str->tags);
            foreach($atags as $atag) {
                $l = explode('-', $atag);
                $this->data['ace_tags'] .= 'var j'.$l[0].' = ace.edit("'.$atag.'"); cnf(j'.$l[0].', "'.$l[0].'");';
            }
            $this->data['ace_tags'] .= '}catch(E) {}}</script>';
        }
        $this->data['recent_blog'] = $this->tutorial_m->get_recent('articles');
        $this->data['recent_tut'] = $this->tutorial_m->get_recent('tutorials');
    	// Load view
    	$this->data['subview'] = 'tutorials_archive';
    	$this->load->view('_layout_main', $this->data);
    }
    
    public function get_by_tag($tag) {
        
        // Count all tutorials
        $this->tutorial_m->set_published();
        $this->data['tutorial'] = NULL;
        $da = $this->tutorial_m->get_count('%#'.$tag.'#%');
        $count = (int) $da->c;
        $perPage = 4;
        
        //Set Pagination
        if ($count > $perPage) {
            $this->load->library('pagination');
            
            $config['base_url'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'. $this->uri->segment(3));
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 4;
            
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(4);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        //Fetch tutorials
//        $this->tutorial_m->set_published();
        $this->db->limit($perPage, $offset);
        $this->db->where('`tags` like \'%#'.$tag.'#%\'');
        $this->data['tutorial'] = $this->tutorial_m->get();
        $this->load->helper('date');
        for($i = 0; $i < count($this->data['tutorial']); $i++) {
            $this->data['tutorial'][$i]->modified = $this->getTime($this->data['tutorial'][$i]->modified);
        }
        $this->data['recent_tut'] = $this->tutorial_m->get_recent('tutorials');
        $this->data['recent_blog'] = $this->tutorial_m->get_recent('articles');
        $this->data['subview'] = 'tutorial';
        $this->load->view('_layout_main', $this->data);
        
    }
    
    private function get_next_prev($id, $category) {
        
        $this->load->helper('security');
        $where = array('id'=>xss_clean($id), 'category'=>$category);
        $next = $this->tutorial_m->get_next($where);
        $prev = $this->tutorial_m->get_prev($where);
        $this->data['next_link'] = count($next) == 1 ? site_url('tutorial/'.$next->slug):NULL;
        $this->data['prev_link'] = count($prev) == 1 ? site_url('tutorial/'.$prev->slug):NULL;
    }


    public function get_tutorial_set($set) {
        
        $this->load->helper('security');
        $set = xss_clean($set);
        $this->data['set_link'] = $set;
        
        // Count all tutorials
        $this->tutorial_m->set_published();
        $da = $this->tutorial_m->get_count_set($set);
        $count = (int) $da->c;
        $perPage = 4;
        
        //Set Pagination
        if ($count > $perPage) {
            $this->load->library('pagination');
//            echo 'rtdjy';
            $config['base_url'] = site_url($this->uri->segment(1).'/'.$this->uri->segment(2).'/'. $this->uri->segment(3));
            $config['total_rows'] = $count;
            $config['per_page'] = $perPage;
            $config['uri_segment'] = 4;
            
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(4);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        //Fetch tutorials
//        $this->tutorial_m->set_published();
        $this->data['tutorial_title'] = $this->tutorial_m->get_titles_of_set($set);
        $this->db->limit($perPage, $offset);
        $this->db->where('`set` like \''.$set.'\'');
        $this->data['tutorial'] = $this->tutorial_m->get();
        $this->load->helper('date');
        for($i = 0; $i < count($this->data['tutorial']); $i++) {
            $this->data['tutorial'][$i]->modified = $this->getTime($this->data['tutorial'][$i]->modified);
        }
        $this->data['recent_tut'] = $this->tutorial_m->get_recent('tutorials');
        $this->data['recent_blog'] = $this->tutorial_m->get_recent('articles');
        // Load view
    	$this->data['subview'] = 'tutorials_set';
    	$this->load->view('_layout_main', $this->data);
        
    }
    
}