<?php
class Course extends MY_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('page_m');
        $this->load->model('article_m');
        $this->load->model('tutorial_m');
        $this->load->model('course_m');
        $this->load->model('frontend_user_m');
        $this->load->library('session');
        // Fetch navigation
        $this->data['loggedin'] = $this->frontend_user_m->loggedin();
        $this->data['nick'] = $this->data['loggedin'] == TRUE ? $this->session->userdata('nick') : 'signin';
        $this->data['menu'] = $this->page_m->get_nested();
        $this->data['archive_lnk'] = $this->page_m->get_archive_link();
        $this->data['meta_title'] = config_item('site_name');
        $this->data['recent'] = $this->course_m->get_recent();
    }

    public function index($id, $slug){
    	// Fetch the course
        $this->course_m->set_published();
        $this->data['course'] = $this->course_m->get($id);
    	
    	// Return 404 if not found
    	count($this->data['course']) || show_404(uri_string());
		
    	// Redirect if slug was incorrect
    	$requested_slug = $this->uri->segment(3);
    	$set_slug = $this->data['course']->slug;
    	if ($requested_slug != $set_slug) {
    		redirect('course/' . $this->data['course']->id . '/' . $this->data['course']->slug, 'location', '301');
    	}
    	
    	// Load view
    	add_meta_title($this->data['course']->title);
    	$this->data['subview'] = 'course';
    	$this->load->view('_layout_main', $this->data);
    }
}