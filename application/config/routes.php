<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = 'page';
$route['homepage'] = 'page/home';
$route['contact'] = 'page/index/$1';
$route['blog/(:num)'] = 'page/index/$1';
$route['blog'] = 'page/index';
$route['tutorial/(:num)'] = 'page/index/$1';
$route['tutorial'] = 'page/index/$1';
$route['course/(:num)'] = 'page/index/$1';
$route['course'] = 'page/index/$1';
$route['blog/(:any)'] = 'article/index/$1';
$route['tutorial/set/(:any)'] = 'tutorial/get_tutorial_set/$1';
$route['tutorial/tag/(:any)'] = 'tutorial/get_by_tag/$1';
$route['blog/tag/(:any)'] = 'article/get_by_tag/$1';
$route['tutorial/(:any)'] = 'tutorial/index/$1';
$route['course/(:num)/(:any)'] = 'course/index/$1/$2';
$route['login'] = 'frontend_user/index';
$route['login/forgot'] = 'frontend_user/forgot_password';
$route['password/reset'] = 'frontend_user/password_reset';
$route['signin'] = 'frontend_user/login';
$route['logout'] = 'frontend_user/logout';
$route['profile/me'] = 'frontend_user/account_edit';
$route['profile/image'] = 'frontend_user/save_image';
$route['profile/(:any)'] = 'frontend_user/account/$1';
$route['signup'] = 'frontend_user/edit';
$route['star/blog/(:num)'] = 'article/star_click/blog/$1';
$route['star/tutorial/(:num)'] = 'tutorial/star_click/blog/$1';
$route['vote/blog/(:num)'] = 'article/vote_click/blog/$1';
$route['vote/tutorial/(:num)'] = 'tutorial/vote_click/tutorial/$1';
$route['(:any)'] = '';
/* End of file routes.php */
/* Location: ./application/config/routes.php */