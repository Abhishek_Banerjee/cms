<?php
class My_Model extends CI_Model
{
    
    protected $_table_name = '';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = '';
    public $rules = array();
    protected $_timestamp = FALSE;


    public function __construct()
    {
        parent::__construct();
    }
    
    public function array_from_post($fields) {
         $data = array();
         foreach ($fields as $field) {
             $data[$field] = $this->input->post($field);
         }
         
         return $data;
    }
    
    public function get_recent($table = '') {
        $q = $this->db->query('select `title`, `slug`, `id` from '.$table.' where `pubdate` <= \''
                .date('Y-m-d').'\' order by `pubdate` desc limit 5;');
        return $q->result();
    }
    
    public function loggedin() {
        return (bool) $this->session->userdata('loggedin') && $this->session->userdata('type') == 1;
    }

    public function get_stars($id) {
        $res = $this->db->query('select * from stars where user_id = ?', array($id));
        return $res->row();
    }
    
    public function get_total_stars($id, $table) {
        $res = $this->db->query('select total_stars from '.$table.' where id = ?;', array($id));
        return $res->row();
    }
    
    public function save_stars($b, $id, $page, $user_id, $count) {
        if($b == 1) {
            $this->db->query('insert into stars values(?, ?, ?);', array($id, $page, $user_id));
            if($page === 'blog') {
                $this->db->query('update articles set total_stars = ? where id = ? limit 1;', array($count+1, $id));
                $res = $count+1;
            }
            else {
                $this->db->query('update tutorials set total_stars = ? where id = ? limit 1;', array($count+1, $id));
                $res = $count+1;
            }
        }
        else {
            $this->db->query('delete from stars where page_id = ? and page = ? and user_id = ? limit 1;', array($id, $page, $user_id));
            
            if($page === 'blog') {
                $this->db->query('update articles set total_stars = ? where id = ? limit 1;', array($count-1, $id));
                $res = $count-1;
            }
            else {
                $this->db->query('update tutorials set total_stars = ? where id = ? limit 1;', array($count-1, $id));
                $res = $count-1;
            }
        }
        
        return $res;
    }
    
//    public function get_total_votes($id, $table) {
//        $res = $this->db->query('select votes from '.$table.' where id = ?;', array($id));
//        return $res->row();
//    }
    
//    public function save_votes($b, $id, $page, $user_id, $count) {
//        if($b == 1) {
//            $prev = $this->db->query('select * from votes where page_id = ? and page = ? and user_id = ? limit 1;'
//                            , array($id, $page, $user_id));
//            $prev = $prev->row();
//            if(count($prev) > 0 && $prev->upvote == 1) {
//                return $count;
//            }
//            if(count($prev) === 0) {
//                $count += 1;
//                $this->db->query('insert into votes values(?, ?, ?, ?);', array($id, $page, $user_id, $b));
//            }
//            else {
////                $s = $prev->row();
//                
//                $count += 1;
//                
//                $this->db->query('delete from votes where page_id = ? and page = ? and user_id = ? limit 1;', array($id, $page, $user_id));
//            }
//            if($page === 'blog') {
//                $this->db->query('update articles set votes = ? where id = ? limit 1;', array($count, $id));
//                $res = $count;
//            }
//            else {
//                $this->db->query('update tutorials set votes = ? where id = ? limit 1;', array($count, $id));
//                $res = $count;
//            }
//        }
//        else {
//            $prev = $this->db->query('select * from votes where page_id = ? and page = ? and user_id = ? limit 1;'
//                            , array($id, $page, $user_id));
//            $prev = $prev->row();
//            if(count($prev) > 0 && $prev->upvote == 0) {
//                return $count;
//            }
//            if(count($prev) === 0) {
//                $count -= 1;
//                $this->db->query('insert into votes values(?, ?, ?, ?);', array($id, $page, $user_id, $b));
//            }
//            else {
//                $count -= 1;
//                $this->db->query('delete from votes where page_id = ? and page = ? and user_id = ? limit 1;', array($id, $page, $user_id));
//            }
//            if($page === 'blog') {
//                $this->db->query('update articles set votes = ? where id = ? limit 1;', array($count, $id));
//                $res = $count;
//            }
//            else {
//                
//                $this->db->query('update tutorials set votes = ? where id = ? limit 1;', array($count, $id));
//                $res = $count;
//            }
//        }
//        
//        return $res;
//    }
    
    public function get_stars_by_id($id, $page, $page_id) {
        $res = $this->db->query('select * from stars where user_id = ? and page = ? and page_id = ? limit 1;', 
                array($id, $page, $page_id));
        return $res->row();
    }
//    
//    public function get_votes_by_id($id, $page, $page_id) {
//        $res = $this->db->query('select * from votes where user_id = ? and page = ? and page_id = ? limit 1;', 
//                array($id, $page, $page_id));
//        return $res->row();
//    }
    
    public function get_ace_tags($id, $page) {
        $q = $this->db->query('select * from `ace_tags` where `id` = ? and `page` like ? limit 1;', array(intval($id), $page));
        return $q->row();
    }
    
    public function get($id = NULL, $single = FALSE)
    {
        if ($id != NULL) {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->where($this->_primary_key, $id);
            $method = 'row';
        }
        elseif($single) {
            $method = 'row';
        }
        else {
            $method = 'result';
        }
        if(!count($this->db->ar_orderby))
        {
            $this->db->order_by($this->_order_by);
        }
        
        return $this->db->get($this->_table_name)->$method();
        
    }
    
    public function get_by($where, $single = FALSE)
    {
        $this->db->where($where);
        return $this->get(NULL, $single);
    }
    
    public function save($data, $id = NULL)
    {
        $this->load->helper('date');
        //Set the timestamp
        if ($this->_timestamp == TRUE) {
//            $now = date('Y-m-d H:i:s');
//            $id || $data['created'] = $now;
            $now = time();
            $data['modified'] = $now;
        }
        //Insert
        if($id === NULL) {
            !isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
            $this->db->set($data);
            $this->db->insert($this->_table_name);
            $id = $this->db->insert_id();
        }
        //Update
        else {
            $filter = $this->_primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->_primary_key, $id);
            $this->db->update($this->_table_name);
        }
        return $id;
    }
    
    public function delete($id)
    {
        $filter = $this->_primary_filter;
        $id = $filter($id);
        
        if (!$id) {
            return FALSE;
        }
        
        $this->db->where($this->_primary_key, $id);
        $this->db->limit(1);
        $this->db->delete($this->_table_name);
    }
}
