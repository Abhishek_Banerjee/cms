<!DOCTYPE html>
<html lang="en">
<head>
<title>404 Page Not Found</title>
<link href="<?php echo base_url('b/css/bootstrap.min.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('b/css/styles.css'); ?>" rel="stylesheet">
<link href="<?php echo base_url('b/assets/favicon.png'); ?>" type="image/x-icon" rel="icon">
</head>
<body style="background: #191819">
    <div class="container" style="height: 700px; background: url('<?php echo site_url('b/assets/404.png');?>') center no-repeat; background-size: 50%">
            <div class="row">
                <div class="col-sm-3" style="margin-top: 2%; ">
                    <a href="<?php echo site_url('');?>" style=" 
	text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);
	color: #333;
	opacity: 0.4;
	font: 300 50px 'Josefin Slab';">Home
                    </a>
                    
                </div>
                <div class="col-sm-3" style="margin-top: 2%;">
                    <a href="<?php echo site_url('/blog');?>" style="
	text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);
	color: #333;
	opacity: 0.4;
        font: 300 50px 'Josefin Slab';">Blog
                    </a>
                </div>
                <div class="col-sm-3" style="margin-top: 2%;">
                    <a href="<?php echo site_url('/tutorial');?>" style="text-shadow: -1px -1px 0px rgba(255,255,255,0.3), 1px 1px 0px rgba(0,0,0,0.8);
	color: #333;
	opacity: 0.4;
        font: 300 50px 'Josefin Slab';">Tutorials
                    </a>
                </div>
            </div>
        <span><img src="<?php echo site_url('b/assets/site_ico_404.png'); ?>"  height="10%"></span>
        </div>
</body>
</html>