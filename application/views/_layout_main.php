<?php $this->load->view('components/page_head'); ?>  
<body >
      <div class="wrapper">
      <header>
          
        <nav class="navbar navbar-fixed-top head" role="navigation">
            <div class="container">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="glyphicon glyphicon-circle-arrow-down" style="color: white;"></span>
                    </button>
                    <a class="navbar-brand my-link" href="<?php echo site_url('') ?>">CMS</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul id = "nav_ul_left" class="nav navbar-nav">
                        <li class=""><a href="<?php echo site_url('blog'); ?>" class="my-link ">Blog</a></li>
                        <li><a href="<?php echo site_url('tutorial'); ?>"class="my-link">Tutorials</a></li>
                    </ul>
                    <?php if ($this->data['loggedin'] == FALSE): ?>
                    <ul id = "nav_ul_id" class="navbar-nav nav navbar-right">
                        <li id = "nav_li_id" >
                            <a id = "nav_ln_id" class="my-link  " href = "<?php echo ''.  site_url('login')?>">
                                <?php echo $nick;?>
                            </a>
                        </li>
                    </ul>
                    <?php else :?>
                    <ul id = "nav_ul_id" class=" nav  navbar-nav navbar-right hovernav">
                        <li id="hov" class="dropdown my-dropdown">
                            <a id="dropdownMenu1" class="my-link-n my-dropdown" data-toggle="dropdown" onclick="fu();">
                                <?php echo $nick;?>
                                <i class="caret"></i>
                            </a>
                            <ul class="dropdown-menu my-link-n"  role="menu" aria-labelledby="dropdownMenu1" >
                                <li role=" presentation">
                                    <a class=" dropdown-toggle my-link-n" role="menuitem" tabindex="-1" href = "<?php echo site_url('profile/'.$nick);?>
                                       ">Account
                                    </a>
                                </li>
                                <li role=" presentation">  
                                    <a class=" dropdown-toggle my-link-n" style="color: #fff;"  role="menuitem" tabindex="-1" href = "<?php echo site_url('profile/me');?>
                                       ">Edit Profile
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a class="dropdown-toggle my-link-n" style="color: #fff;"  role="menuitem" tabindex="-1" href = "<?php echo site_url('logout'); ?>
                                       ">Log Out
                                    </a>
                                    </li>
                            </ul>
                        </li>
                    </ul>
                        <?php endif; ?>
                </div>
            </div>
        </nav>
          
      </header>
      
      <div class="container">
          <br><br>
          <script>
              $("#hov").hover(function() {
    $(this).addClass("open");}, function() {
    $(this).removeClass("open");
}
);
function fu() {
    window.location.replace("<?php echo site_url('profile/'.$nick);?>");
}
          </script>
              <?php $this->load->view('templates/'.$subview); ?>
      </div>
      </div>
      
<?php $this->load->view('components/page_tail'); ?>
