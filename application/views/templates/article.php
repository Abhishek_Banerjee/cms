<div class="row" style=" margin-top: 4%; margin-bottom: 4%;">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url();?>">Home</a></li>
                    <li><a href="<?php echo site_url('blog');?>">Blog</a></li>
                    <li class="active"><?php echo $article->title;?></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div style="margin-right: 2%">
        
        <article>
            <div class="row">
                <div id="error-4"></div>
                <div class="col-xs-11">
                    
                    <h2><?php echo e($article->title);?></h2>
                    <!--<small><i class="glyphicon glyphicon-time"></i> </small>-->
                    <?php 
                        $array = explode('#', $article->tags);
                        echo '<div class="">';
                        foreach ($array as $k) {
                            if($k != '')
                            echo '<a class="label label-default" style=" margin-bottom: 1%" href="'.  
                                    site_url('blog/tag/'.$k).'">'.$k.'</a> ';
                        }
                        echo '</div>';
                        ?>
                    <p class="" style="margin-top: 2%; font-family: cursive, serif;">by <?php if($author->name === 'Anonymous'):?>
                            <?php echo $author->name;?>, <?php echo e($modified);?>
                            <?php else:?>
                            <a href="<?php echo site_url('profile/'.$author->nick);?>"><?php echo $author->name;?></a>, <?php echo e($modified);?>
                            <?php endif;?>
                    </p>
                </div>
                <div id="star" class="col-xs-1">
                    <a onclick="star();"><img id="star_img" src="<?php echo site_url('b/assets/'.$img);?>" width="36px" height="36px"></a>
                    <p class="star-t"><?php echo $article->total_stars;?></p>
                </div>
            </div>
            <p style="font-family: 'Josefin Slab', serif; font-size: 21px;"><?php echo $article->body;?></p>
        </article>
    </div>
</div>
    <?php echo $this->load->view('sidebar');?>
    <?php echo $this->load->view('templates/comments');?>
    </div>
<script>
    
    cb = document.getElementById('star_img').src === '<?php echo site_url();?>b/assets/star2.png' ? 1:0;
    
function star() {
//    alert('dskjb');
    <?php if(!$this->session->userdata('id')): ?>
//            alert('cd');
    $('#error-4').html('<div class="alert alert-dismissable alert-danger">Log in to star'+
'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
    <?php else:?>
        $('#star').html('<img id=\"gif_load2\"src="<?php echo base_url('b/loading.gif');?>" style=\"margin: 0px auto;\" />');
        <?php $CI =& get_instance();?>
        $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('star/blog/'.$CI->uri->segment(2)); ?>",
                    data: "add="+cb
                }).done(function (data) {
                    if(data !== 'error') {
//                        alert(data);
                        cb = 1 - cb;
                        var im = cb === 0 ? 'star.png' : 'star2.png';
                        $('#star').html("<a onclick=\"star();\"><img id=\"star_img\" src=\"<?php echo site_url();?>b/assets/"+im+"\" width=\"36px\" height=\"36px\"></a>"+
                    "<p class=\"star-t\">"+data.toString()+"</p>");
                    }
                    else {
                        alert(data);
                    }
                });
                
    <?php endif;?>
}

function vote(d) {
    <?php if(!$this->session->userdata('id')): ?>
//            alert('cd');
    $('#error-4').html('<div class="alert alert-dismissable alert-danger">Log in to vote'+
'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>');
    <?php else:?>
        $('#vote').html('<img id=\"gif_load2\"src="<?php echo base_url('b/loading.gif');?>" style=\"margin: 0px auto;\" />');
        <?php $CI =& get_instance();?>
        $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('vote/blog/'.$CI->uri->segment(2)); ?>",
                    data: "upvote="+d
                }).done(function (data) {
                    if(data !== 'error') {
                        var im;
                        $('#vote').html("<a class=\"star-t\" onclick=\"vote(1);\"><img src=\"<?php echo site_url('b/assets/up21.png');?>\""+
                                " width=\"36px\" height=\"20px\"></a>"+
                    "<p class=\"star-t\" style=\"font-family: 'Josefin Slab', serif; font-size: 21px;\">"+data+
                    "<a class=\"star-t\" onclick=\"vote(0);\"><img src=\"<?php echo site_url('b/assets/down21.png');?>\" width=\"36px\" height=\"20px\"></a></p>");
                    }
                    else {
                        alert(data);
                    }
                });
                
    <?php endif;?>
}
</script>
        <?php echo isset($ace_tags)? $ace_tags:'';?>
