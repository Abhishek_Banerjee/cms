<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="<?php echo base_url('b/js/jquery.js'); ?>"></script>
    <link href="<?php echo base_url('b/assets/favicon.png'); ?>" type="image/x-icon" rel="icon">
    <link rel="shortcut icon" href="<?php echo base_url('b/assets/ico/favicon.ico');?>">
    <link href="<?php echo base_url('/b/css/carousel.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('b/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('b/css/styles.css'); ?>" rel="stylesheet">
    <title>CMS</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('b/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
  </head>
<!-- NAVBAR ================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container" id="nav-div-1">
          <nav id = "fixed_nav" style="margin-top: -55px;" class="navbar navbar-fixed-top head" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="glyphicon glyphicon-circle-arrow-down" style="color: white;"></span>
                    </button>
                    <a class="navbar-brand my-link" href="<?php echo site_url('') ?>">CMS</a>
                </div>
                
                <div id = "nav_ul_left" class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul id = "nav_ul_left" class="nav navbar-nav">
                        <li class=""><a href="<?php echo site_url('blog'); ?>" class="my-link ">Blog</a></li>
                        <li><a href="<?php echo site_url('course_archive'); ?>"class="my-link">Course</a></li>
                        <li><a href="<?php echo site_url('tutorial'); ?>"class="my-link">Tutorials</a></li>
                    </ul>
                    <?php if ($this->data['loggedin'] == FALSE): ?>
                    <ul id = "nav_ul_id" class="navbar-nav nav navbar-right">
                        <li id = "nav_li_id" >
                            <a id = "nav_ln_id" class="my-link" href = "<?php echo site_url('login'); ?>">
                                <?php echo $nick; ?>
                            </a>
                        </li>
                    </ul>
                        <?php else :?>
                    <ul id = "nav_ul_id" class="my-link-n nav  navbar-nav navbar-right hovernav">
                        <li id="hov" class="dropdown my-dropdown">
                            <a id="dropdownMenu1" class="my-link-n my-dropdown" data-toggle="dropdown">
                                <?php echo $nick;?>
                                <i class="caret"></i>
                            </a>
                            <ul class="dropdown-menu my-link-n"  role="menu" aria-labelledby="dropdownMenu1" >
                                <li role=" presentation">
                                    <a class=" dropdown-toggle my-link-n" role="menuitem" tabindex="-1" href = "<?php echo site_url('profile/'.$nick);?>">
                                        Account
                                    </a>
                                </li>
                                <li role=" presentation">  
                                    <a class=" dropdown-toggle my-link-n" style="color: #fff;"  role="menuitem" tabindex="-1" href = "<?php echo site_url('profile/me');?>
                                       ">Edit Profile
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a class="dropdown-toggle my-link-n" style="color: #fff;"  role="menuitem" tabindex="-1" href = "<?php echo site_url('logout'); ?>
                                       ">Log Out
                                    </a>
                                    </li>
                            </ul>
                        </li>
                    </ul>
                        <?php endif; ?>
                        <?php // echo get_menu($menu, FALSE, $this->data['nick']);?>
                </div>
            </div>
        </nav>
      </div>
    </div>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img data-src="" alt="First slide">
          <div class="">
            <div class="carousel-caption">
              <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide">
          <div class="">
            <div class="carousel-caption">
              <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide">
          <div class="">
            <div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->
<script>
              $("#hov").hover(function() {
    $(this).addClass("open");}, function() {
    $(this).removeClass("open");
}
);
</script>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another  to center all the content. -->

    <div class="marketing">

      <!-- Three columns of text below the carousel -->
      <div class="container">
      <div class="row">
          
        <div class="col-lg-4">
          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
          <h2>Heading NN</h2>
          <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
          <h2>Heading</h2>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" data-src="holder.js/140x140" alt="Generic placeholder image">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
          </div>
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->
      <div style="background: #000">
          <div class="container">
      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>
          </div>
      </div>
<div class="container">
      <div class="row featurette">
          
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
          </div>
      </div>
<div class="container">
      <div class="row featurette">
          
        <div class="col-md-7">
          <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
          <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
          </div>
      </div>
      <!-- /END THE FEATURETTES -->
    </div><!-- /. -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
       $(document).ready(
                    function poll() {
                        var pos = $(window).scrollTop();
                        if(pos < 55) {b = true;}
                        else {b = false;}
                    }
                    );
    function nav_bar_static() {
        
        var pos = $(window).scrollTop();
        
        if(pos < 55 && window.b) {
            window.b = false;
            $('#fixed_nav').animate({
        'marginTop' : "-55px" //moves down
        });
//            $('#fixed_nav').removeClass('navbar-fixed-top');
//            $('#nav-div-1').removeClass('container');
//            $('#fixed_nav').addClass('navbar-static-top');
            
        }
        else if(pos > 55 && !window.b)  {
            window.b = true;
            $('#fixed_nav').animate({
        'marginTop' : "0px" //moves down
        });
//            $('#fixed_nav').removeClass('navbar-static-top');
//            $('#nav-div-1').addClass('container');
//            $('#fixed_nav').addClass('navbar-fixed-top');
            
        }
    }
//    var prevPos = $(window).scrollTop();
    $(window).scroll(function() {
//        var pos = $(window).scrollTop();
//        
//        if(pos > 50 && prevPos < 50) {
            nav_bar_static();
//            prevPos = pos;
//        }
//        if(pos < 50 && prevPos > 50) {
//            nav_bar_static(false);
//            prevPos = pos;
//        }
    }
            );
    
    
    </script>
    <script src="<?php echo base_url('b/js/bootstrap.min.js');?>"></script>
        <?php $this->load->view('components/page_tail');?>
