
    <div class="row my-div-1">
        <div class="col-lg-12">
        <br><br>
        <div class="row">
            <div class="col-lg-3" >
                <img src="<?php echo substr($useri['image'], 0, 4) === 'img/' ? base_url($useri['image']) : $useri['image'];?>" 
                     width="128" height="128" class="comment-img">
            </div>
            
            <div class="col-lg-6 my-div-1" style="margin-bottom: 4%">
                <ol class="list-group">
                    <li class="list-group-item"><span class="glyphicon glyphicon-user"></span><strong>  User Info</strong></li>
                    <li class="list-group-item"><div>
                                User Nick Name: <i><?php echo $useri['nick'];?></i></div>
                        </li>
                        <li class="list-group-item"><div>
                                User Name     : <i><?php echo $useri['name'];?></i>
                        </li>
                        <li class="list-group-item"><div>
                                Bio     : <i><?php echo $useri['bio'];?></i></li>
                </ol></div>
            </div>
            <?php if($this->data['loggedin'] && $this->session->userdata('nick') === $useri['nick']):?>
            <div class="col-lg-3"><a class="btn btn-default" href="<?php echo base_url('profile/me');?>">Edit</a></div>
            <?php endif;?>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 my-div-1" style="margin-bottom: 4%">
            </div>
        </div>
        </div>