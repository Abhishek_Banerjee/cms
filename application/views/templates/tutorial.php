<div class="row" style="margin-top: 4%">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url();?>">Home</a></li>
                <li class="active">Tutorial</li>
            </ol>
        </div>
    </div>
<div class="col-lg-9">    
    <div class="row">
        <div class="span9"><?php if (isset($tutorial[0])){echo get_excerpt_t($tutorial[0]);}?></div>
    </div>
    <div class="row">
        <div class="span9"><?php if (isset($tutorial[1]))echo get_excerpt_t($tutorial[1]);?></div>
    </div>
    <div class="row">
        <div class="span9"><?php if (isset($tutorial[2]))echo get_excerpt_t($tutorial[2]);?></div>
    </div>
    <div class="row">
        <div class="span9"><?php if (isset($tutorial[3]))echo get_excerpt_t($tutorial[3]);?></div>
    </div>
    <div class="row">
        <div class="span9"><?php if (isset($tutorial[4]))echo get_excerpt_t($tutorial[4]);?></div>
    </div>
    <div class="row">
        <div class="span9"><?php if (isset($tutorial[5]))echo get_excerpt_t($tutorial[5]);?></div>
    </div>
</div>
    <?php echo $this->load->view('sidebar');?>
<div class="row">
    <div class="col-lg-9">
        <?php if($pagination):?>
        <section><?php echo $pagination;?></section>
        <?php endif;?>
    </div>
</div>
</div>
