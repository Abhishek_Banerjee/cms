<div class="row" style="margin-top: 4%;">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo site_url();?>">Home</a></li>
                    <li><a href="<?php echo site_url('tutorial');?>">Tutorial</a></li>
                    <li><a href="<?php echo site_url('tutorial/set/'.$tutorial->set);?>"><?php echo $tutorial->set;?></a></li>
                    <li class="active"><?php echo $tutorial->title;?></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="col-sm-9">    
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="list-group" style="margin-top: 14%;">
                            <a  class="list-group-item"  href="<?php echo site_url('tutorial/set/'.$tutorial->set);?>">
                                <span class="glyphicon glyphicon-list"></span>  
                                <strong>  <?php echo $tutorial->set;?></strong></a>

                                <?php
                            foreach ($tutorial_title as $t):?>
                            <a class="list-group-item" href="<?php echo site_url('tutorial/'.$t->slug); ?>"><?php echo $t->title; ?>
                                    </a>
                                    <?php endforeach;?>

                        </ul>
                    </div>
                    <div class="col-sm-9">
                <h2><?php echo e($tutorial->title);?></h2>
                
                <?php 
                $array = explode('#', $tutorial->tags);
                echo '<div class="">';
                foreach ($array as $k) {
                    if($k != '')
                    echo '<a class="label label-default" style=" margin-bottom: 1%" href="'.  
                            site_url('tutorial/tag/'.$k).'">'.$k.'</a> ';
                }
                echo '</div>';
                ?>
                <p class="" style="margin-top: 2%; font-family: cursive, serif;">by <?php if($author->name === 'Anonymous'):?>
                    <?php echo $author->name;?>, <?php echo e($modified);?>
                    <?php else:?>
                    <a href="<?php echo site_url('profile/'.$author->nick);?>"><?php echo $author->name;?></a>, <?php echo e($modified); ?>
                    <?php endif;?>
                </p>
                <p style="font-family: 'Josefin Slab', serif; font-size: 21px; ">
                <?php echo $tutorial->body;?>
                </p>
                <?php 
                $c = isset($prev_link) ? '':'disabled';
                $e = isset($next_link) ? '':'disabled';
                $cc = isset($prev_link) ? $prev_link : '';
                $ee = isset($next_link) ? $next_link : '';
                        echo '<ul class="pager">'
                        . '<li class="previous '.$c.'"><a href="'.$cc.'">&larr; Previous</a></li>'
                                . '<li class="next '.$e.'"><a href="'.$ee.'">Next &rarr;</a></li></ul>';
                ?>
                   </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->load->view('sidebar');?>
    </div>
    <?php echo $this->load->view('templates/comments');?>
</div>
<?php echo isset($ace_tags)? $ace_tags:'';?>