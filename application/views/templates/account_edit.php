
<script>
function getImage(i) {
    var b = "<?php echo site_url('img');?>";
    b = b + "/default"+i+".png";
    document.getElementById("image-id").src = b;
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('profile/image');?>",
        data: "image="+"img/default"+i+".png"
}).done(function (d) {
//    alert(d);
});
}
</script>
    <div class="row my-div-1">
        <div class="col-lg-12">
        <br><br>
        <?php echo form_open(site_url('profile/me'));?>
        <div class="row">
            <div class="col-lg-3">
                <img id="image-id" src="<?php echo substr($useri['image'], 0, 4) === 'img/' ? base_url($useri['image']) : $useri['image'];?>"
                     class="comment-img"  width="128" height="128" style="margin-bottom: 5%;">
                <br>
                <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Change</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Choose an image</h4>
      </div>
      <div class="modal-body">
          <?php for($i = 1; $i < 8; $i++):?>
          <a onclick="getImage(<?php echo $i;?>)"><img src="<?php echo site_url('img/default'.$i.'.png');?>" width="120" height="120" style="margin-bottom: 1%;"></a>
          <?php endfor;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
            </div>
            <div class="col-lg-6 my-div-1" style="margin-bottom: 0%">
                
                <?php $e = validation_errors(); 
                echo $e !== ''? '<div class="alert alert-dismissable alert-danger">'. $e .
'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>':'';?>
                
                User Nick Name: <input class="form-control" name="nick" type="text" value="<?php echo $useri['nick'];?>">
                <br>
                User Name: <input class="form-control" name="name" type="text" value="<?php echo $useri['name'];?>">
                <br>
                Short Bio: <input class="form-control" name="bio" type="text" value="<?php echo $useri['bio'];?>">
                <br>
                <input class="btn btn-primary" type="submit" value="Save">
            </div>
        </div>
        <?php echo form_close();?>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 my-div-1" style="margin-bottom: 1%">
                
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 my-div-1" style="margin-bottom: 1%">
                
            </div>
        </div>
        
        </div>
    </div>

