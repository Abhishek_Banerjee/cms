<div class="row" style="margin-top: 4%;">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="<?php echo site_url();?>">Home</a></li>
                <li><a href="<?php echo site_url('tutorial');?>">Tutorial</a></li>
                <li class="active"><?php echo $set_link;?></li>
            </ol>
        </div>
    </div>
    <div class="col-lg-3 col-md-3" style="margin-top: 2%;">    
    <div class="row">
        <ul class="list-group">
            <a  class="list-group-item"  href="<?php echo site_url('tutorial/set/'.$set_link);?>">
                <span class="glyphicon glyphicon-list"></span>  
                <strong>  <?php echo $set_link;?></strong></a>
        
                <?php
            foreach ($tutorial_title as $t):?>
            <a class="list-group-item" href="<?php echo site_url('tutorial/'.$t->category.'/'.$t->slug); ?>"><?php echo $t->title; ?>
                    </a>
                    <?php endforeach;?>

        </ul>
    </div>
</div>
    <div class="col-lg-6 col-md-6">    
    <div class="">
        <div class=""><?php if (isset($tutorial[0])){echo get_excerpt_t($tutorial[0]);}?></div>
    </div>
    <div class="">
        <div class=""><?php if (isset($tutorial[1]))echo get_excerpt_t($tutorial[1]);?></div>
    </div>
    <div class="">
        <div class=""><?php if (isset($tutorial[2]))echo get_excerpt_t($tutorial[2]);?></div>
    </div>
    <div class="">
        <div class=""><?php if (isset($tutorial[3]))echo get_excerpt_t($tutorial[3]);?></div>
    </div>
    <div class="">
        <div class=""><?php if (isset($tutorial[4]))echo get_excerpt_t($tutorial[4]);?></div>
    </div>
    <div class="">
        <div class=""><?php if (isset($tutorial[5]))echo get_excerpt_t($tutorial[5]);?></div>
    </div>
</div>
    <?php echo $this->load->view('sidebar');?>
</div>
<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
        <?php if($pagination):?>
        <section><?php echo $pagination;?></section>
        <?php endif;?>
    </div>
</div>
</div>