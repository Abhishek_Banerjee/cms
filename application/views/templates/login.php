<div class="row">
    <div class="col-lg-12">
        
        <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-1"></div>
            <div class="col-lg-4  col-md-4 col-sm-4">
                <div id="error-1"></div>
                <!--Sign in Area-->
                <h3>Sign in</h3>
                            <div class="mt-1" id="email1">
                                <input  id="email_in"  class="form-control" type="text" name="email" placeholder="Email" onclick="hideE(1)">
                            </div>
                            <div class="mt-1" id="password1" class="">
                                <input id="password_in" class="form-control" type="password" name="password" placeholder="password" onclick="hideE(2)">
                            </div>
                <input id="sign_in" class="btn btn-primary mt-1" value="Sign in" type="submit" onclick="login()">
                <a href="<?php echo isset($authUrl)? $authUrl:'';?>" style="text-decoration: none;">
                                <img height="40" width="40" src="<?php echo base_url('b/assets/google.png'); ?>"/>
                                <?php //if(isset($useri)): //dump($useri); endif; ?>
                            </a>
                            <a href="<?php echo site_url('/login/forgot');?>">forgot password</a>
            </div>
            <div class="col-lg-1  col-md-1 col-sm-1"></div>
            <div class="col-lg-1  col-md-1 col-sm-1"></div>  
            <div class="col-lg-4  col-md-4 col-sm-4" >
                    <!--Sign up Area-->
                    <div id="error-2"></div>
                    <h3>Sign up</h3>
                    <div id="email2" class="mt-1">
                        <input id="email_in_2" name="email" type="text" class="form-control" placeholder="Email" onclick="hideE(3)">
                    </div>
                    <div id="nick1" class="mt-1">
                        <input id="nick_in" name="nick" type="text" class="form-control" placeholder="Nick Name" onclick="hideE(4)">
                    </div>
                    <div id="name1" class="mt-1">
                        <input id="name_in" name="name" type="text" class="form-control" placeholder="Name" onclick="hideE(5)">
                    </div>
                    <div id="password2" class="mt-1">
                        <input id="password_in_2" name="password" type="password" class="form-control" placeholder="password" onclick="hideE(6)">
                    </div>
                    <div id="confirm_pass" class="mt-1">
                        <input id="password_confirm" name="password_confirm" type="password" class="form-control" placeholder="password confirm" onclick="hideE(7)">
                    </div>
                    <input id="sign_up" name="submit" type="submit" class="btn btn-primary mt-1" value="Sign up" onclick="signup()">
                    <!--<input name="submit" type="submit" class="btn btn-primary mt-1" value="Sign up">-->
            </div>
            <div class="col-lg-1"></div>  
        </div>
        
    </div>
</div>
<div class="" style="margin-bottom: 20px;"></div>
<script>
    $(document).ready( function st() {
        $('body').on('keypress', '#password_in', function(args) {
    if (args.keyCode == 13) {
        $('#sign_in').click();
        return false;
    }
});
$('body').on('keypress', '#password_confirm', function(args) {
    if (args.keyCode == 13) {
        $('#sign_up').click();
        return false;
    }
});
    }
            );
    function error_div(i, d) {
        $(i).html("<div style=\"margin-top: 12%;\" class=\"alert alert-dismissable alert-danger\">"+d+
"<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button></div>");
    }
    function login() {
        if(document.getElementById('email_in').value != ''
                        && document.getElementById('password_in').value != '') {
//                $('#email1').html('<img id=\"gif_load\"src="<?php // echo base_url('b/loading.gif'); ?>" style=\" margin: 0px auto;\" />');
//                $('#email_in').fadeOut(100);
//                $('#password_in').fadeOut(100);
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('/signin');?>",
                    data: "email="+$('#email_in').val()+"&password="+$('#password_in').val()
}).done(function (d) {
//    alert(d);
if(d === "success") {
    window.location.replace("<?php echo site_url();?>");
}
else {
if(d === "Invalid Email") {
    $('#email_in').val("");
    }
    $('#password_in').val("");
//    if(d === "fail") {
    error_div("#error-1", d);
    showE(1);
//    }
    //$('#email1').html('<img id=\"gif_load\"src="<?php // echo base_url('b/loading.gif'); ?>" style=\" margin: 0px auto;\" />');
//    $('#email_in').fadeIn(100);
//    $('#password_in').fadeIn(100);
                }
                });
                }
                else {
            showE(1);
        }
    }
function hideE(i) {
    if(i === 1) {
        $('#email1').removeClass('has-error');
        return;
    }
    if(i === 2) {
        $('#password1').removeClass('has-error');
    }
    if(i === 3) {
        $('#email2').removeClass('has-error');
    }
    if(i === 4) {
        $('#nick1').removeClass('has-error');
    }
    if(i === 5) {
        $('#name1').removeClass('has-error');
    }
    if(i === 6) {
        $('#password2').removeClass('has-error');
    }
    if(i === 7) {
        $('#confirm_pass').removeClass('has-error');
    }
    
    
}
function showE(i) {
    if(i === 1) {
        if(document.getElementById('email_in').value === '') {
            $('#email1').addClass("has-error");
            return ;
        }
        if(document.getElementById('password_in').value === '') {
            $('#password1').addClass("has-error");
            return ;
        }
    }
    else if(i === 2) {
        if(document.getElementById('email_in_2').value === '') {
            $('#email2').addClass("has-error");
        }
        if(document.getElementById('nick_in').value === '') {
            $('#nick1').addClass("has-error");
        }
        if(document.getElementById('name_in').value === '') {
            $('#name1').addClass("has-error");
        }
        if(document.getElementById('password_confirm').value !== document.getElementById('password_in_2').value) {
            error_div("#error-2","Passwords do not match");
            $('#confirm_pass').addClass("has-error");
            $('#password2').addClass("has-error");
            return;
        }
        if(document.getElementById('password_in_2').value === '') {
            $('#password2').addClass("has-error");
        }
        if(document.getElementById('password_confirm').value === '') {
            $('#confirm_pass').addClass("has-error");
        }
        
    }
}
function signup() {
    if(document.getElementById('email_in_2').value === ''
    || document.getElementById('password_in_2').value === ''
    || document.getElementById('nick_in').value === ''
    || document.getElementById('name_in').value === ''
    || document.getElementById('password_confirm').value === '') {
        error_div("#error-2","All Fields are Required.");
        showE(2);
    }
    else if(document.getElementById('password_confirm').value !== document.getElementById('password_in_2').value) {
        $('#password_confirm').val("");
        $('#password_in_2').val("");
        error_div("#error-2","Passwords do not match");
        showE(2);
    }
    else {
    //$('#email1').html('<img id=\"gif_load\"src="<?php //echo base_url('b/loading.gif'); ?>" style=\" margin: 0px auto;\" />');
//    $('#email_in_2').fadeOut(100);
//    $('#password_in_2').fadeOut(100);
//    $('#password_confirm').fadeOut(100);
//    $('#name_in').fadeOut(100);
//    $('#nick_in').fadeOut(100);
    $.ajax({
        type: "POST",
        url: "<?php echo site_url('/signup');?>",
        data: "email="+$('#email_in_2').val()+"&password="+$('#password_in_2').val()
        +"&password_confirm="+$('#password_confirm').val()+"&nick="+$('#nick_in').val()+"&name="+$('#name_in').val()
}).done(function (d) {
//    alert(d);
    
    if(d !== "success") {
        $('#email_in_2').val("");
        $('#nick_in').val("");
        $('#name_in').val("");
        $('#password_in_2').val("");
        $('#password_confirm').val("");
        error_div("#error-2", d);
        showE(2);
    }
    else {
        window.location.replace("<?php echo site_url('profile/me');?>");
    }
                });
    }
    
}
</script>