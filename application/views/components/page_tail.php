<br><br>
<div class="my-footer footer">
    <div class="container">
        <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-9"><br><br></div>
                <div class="col-lg-3" style="text-align: right"><br><br><a href="#">TOP</a></div>
            </div>
        </div>
            </div>
        <div class="row">
        <div class=" col-lg-12">
            <div class="row">
                <div class="col-lg-4">
                    <h5 class="my-right">Courses</h5>
                    <ul class="my-ul">
                        <li><a class="my-link-2" href="#">some Link</a></li>
                        <li><a class="my-link-2"  href="#">some Link</a></li>
                        <li><a class="my-link-2" href="#">some Link</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h5 class="my-right">Blog</h5>
                    <ul class="my-ul">
                        <li><a class="my-link-2" href="%">some Link</a></li>
                        <li><a class="my-link-2" href="%">some Link</a></li>
                        <li><a class="my-link-2" href="%">some Link</a></li>
                    </ul>
                    
                </div>
                <div class="col-lg-4">
                    <h5 class="my-right">Tutorials</h5>
                    <ul class="my-ul">
                        <li><a class="my-link-2" href="%">link</a></li>
                        <li><a class="my-link-2" href="%">link</a></li>
                    </ul>
                    
                </div>
        </div>
        </div>
        </div>
        <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12"><br><p>&copy; Abhishek Banerjee 2014 All rights reserved</p><br></div>
            </div>
        </div>
        </div>
    </div>
    
</div>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('b/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo site_url('b/js/app.js');?>" type="text/javascript" charset="utf-8"></script>
    <?php if(isset($ace_tags)):?>
    <script src="http://ajaxorg.github.io/ace-builds/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
    <?php endif;?>
  </body>
</html>