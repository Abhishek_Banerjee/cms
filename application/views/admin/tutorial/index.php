<section>
    <h2>tutorial</h2>
    <a href="Tutorial/edit"><i class="glyphicon glyphicon-plus"></i> Add an tutorial</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>Pub date</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        
        <tbody>
            <?php if(count($tutorials)): foreach ($tutorials as $tutorial):?>
            
            <tr>
                <td><?php echo anchor('admin/tutorial/edit/'.$tutorial->id, $tutorial->title); ?></td>
                <td><?php echo $tutorial->pubdate; ?></td>
                <td><?php echo btn_edit('admin/tutorial/edit/'.$tutorial->id); ?></td>
                <td><a href="tutorial/delete/<?php echo $tutorial->id; ?>" onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?'); ">
                       <i class="glyphicon glyphicon-remove"></i></a></td>
            </tr>
            <?php endforeach; ?>
                <?php else:?>
        <td colspan="3">No tutorials were found.</td>
            <?php endif; ?>
        </tbody>
    </table>
</section>