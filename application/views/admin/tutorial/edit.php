    <h3><?php echo empty($tutorial->id)? 'Add a tutorial':'Edit tutorial '.$tutorial->title ?></h3>
    <?php echo validation_errors(); ?>
    <?php echo form_open(); ?>
    <table class="table">
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'pubdate',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Publication Date'), set_value('pubdate', $tutorial->pubdate)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'title',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Title'), set_value('title', $tutorial->title)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'slug',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Slug'), set_value('slug',$tutorial->slug)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'tags',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Tags Separated by a #'), set_value('tags',$tutorial->tags)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'set',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Which tutorial set it belongs'), set_value('set',$tutorial->set)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'category',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Category'), set_value('category',$tutorial->category)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_textarea(array('name' => 'body',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Body'), set_value('body',$tutorial->body)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_submit(array('name' => 'submit',
                                             'value' => 'Save',
                                             'class' => 'btn btn-primary')); ?></td>
            
        </tr>
    </table>
    <?php echo form_close();?>