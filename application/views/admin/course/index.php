<section>
    <h2>course</h2>
    <a href="course/edit"><i class="glyphicon glyphicon-plus"></i> Add an course</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>Pub date</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </thead>
        
        <tbody>
            <?php if(count($courses)): foreach ($courses as $course):?>
            
            <tr>
                <td><?php echo anchor('admin/course/edit/'.$course->id, $course->title); ?></td>
                <td><?php echo $course->pubdate; ?></td>
                <td><?php echo btn_edit('admin/course/edit/'.$course->id); ?></td>
                <td><a href="course/delete/<?php echo $course->id; ?>" onclick="return confirm('You are about to delete a record. This cannot be undone. Are you sure?'); ">
                       <i class="glyphicon glyphicon-remove"></i></a></td>
            </tr>
            <?php endforeach; ?>
                <?php else:?>
        <td colspan="3">No courses were found.</td>
            <?php endif; ?>
        </tbody>
    </table>
</section>