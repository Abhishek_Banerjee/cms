    <h3><?php echo empty($course->id)? 'Add a course':'Edit course '.$course->title ?></h3>
    <?php echo validation_errors(); ?>
    <?php echo form_open(); ?>
    <table class="table">
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'pubdate',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Publication Date'), set_value('pubdate', $course->pubdate)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'title',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Title'), set_value('title', $course->title)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'slug',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Slug'), set_value('slug',$course->slug)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'syllabus',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Syllabus Link'), set_value('syllabus',$course->syllabus)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'tags',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Tags Separated by a space'), set_value('tags',$course->tags)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'instructor',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Instructor'), set_value('set',$course->instructor)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'image',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Image link'), set_value('set',$course->image)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_input(array('name' => 'time',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Time separated by space'), set_value('time',$course->time)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_textarea(array('name' => 'links',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Tutorial Links separated by space'), set_value('body',$course->links)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_textarea(array('name' => 'summary',
                                            'type' => 'text',
                                            'class' => 'form-control',
                                            'placeholder' => 'Summary'), set_value('summary',$course->summary)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo form_submit(array('name' => 'submit',
                                             'value' => 'Save',
                                             'class' => 'btn btn-primary')); ?></td>
            
        </tr>
    </table>
    <?php echo form_close();?>