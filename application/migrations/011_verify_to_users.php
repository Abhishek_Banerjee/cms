<?php
class Migration_Verify_to_users extends CI_Migration {
    
    public function up() {
        $fields = (array(
			'verify' => array(
                            
				'type' => 'BOOL',
				'default' => 0
			),
		));
        
        $this->dbforge->add_column('frontend_users', $fields);
    }
    
    public function down()
    {
        $this->dbforge->drop_column('frontend_users', 'verify');
    }
}
