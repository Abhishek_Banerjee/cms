<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_Courses extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
                        'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
                        'image' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
                        'instructor' => array(
				'type' => 'VARCHAR',
				'constraint' => '50'
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
                        'tags' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
                        'syllabus' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'pubdate' => array(
				'type' => 'DATE'
			),
                        'links' => array(
				'type' => 'VARCHAR',
                                'constraint' => '1000'
			),
                        'time' => array(
				'type' => 'VARCHAR',
                                'constraint' => '100'
			),
                        'created' => array(
				'type' => 'DATETIME'
			),
                        'modified' => array(
				'type' => 'DATETIME'
			),
                        'summary' => array(
				'type' => 'VARCHAR',
                                'constraint' => '1000'
			),
		));
                $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('courses');
	}

	public function down()
	{
		$this->dbforge->drop_table('courses');
	}
}