<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_ace_tags extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE
			),
                        'tag' => array(
				'type' => 'VARCHAR',
				'constraint' => 200
                            ),
		));
                $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ace_tags');
	}

	public function down()
	{
		$this->dbforge->drop_table('ace_tags');
	}
}
