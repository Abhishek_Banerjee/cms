<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_frontend_users extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 5,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
                        'email' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
                                'unique' => TRUE
			),
			'password' => array(
				'type' => 'VARCHAR',
				'constraint' => '128'
                                
			),
                        'nick' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
                                'unique' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
		));
                $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('frontend_users');
	}

	public function down()
	{
		$this->dbforge->drop_table('frontend_users');
	}
}