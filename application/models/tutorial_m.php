<?php 
class Tutorial_m extends My_Model {
    
    protected $_table_name = 'Tutorials';
    protected $_order_by = 'pubdate desc, id desc';
    protected $_timestamp = TRUE;
    public $rules = array(
        'pubdate' => array('field' => 'pubdate',
                         'label' => 'Publication Date',
                         'rules' => 'trim|required|exact_length[10]|xss_clean'),
        'title' => array('field' => 'title',
                         'label' => 'Title',
                         'rules' => 'trim|required|max_length[100]|xss_clean'),
        'slug' => array('field' => 'slug',
                         'label' => 'Slug',
                         'rules' => 'trim|required|max_length[100]|url_title|xss_clean'),
        'tags' => array('field' => 'tags',
                         'label' => 'Tags',
                         'rules' => 'trim|required|xss_clean'),
        'set' => array('field' => 'set',
                         'label' => 'Set',
                         'rules' => 'trim|required|xss_clean'),
        'category' => array('field' => 'category',
                         'label' => 'Category',
                         'rules' => 'trim|required|max_length[100]|xss_clean'),
        'body' => array('field' => 'body',
                         'label' => 'Body',
                         'rules' => 'trim|required'),
    );

    public function get_author($id) {
        $q = $this->db->query('select name, image, bio, nick from frontend_users where id = ?;', $id);
        return $q->row();
    }
    
    public function get_new() {
        $tutorial = new stdClass();
        $tutorial->title = '';
        $tutorial->slug = '';
        $tutorial->tags = '';
        $tutorial->set = '';
        $tutorial->category = '';
        $tutorial->body = '';
        $tutorial->pubdate = date('Y-m-d');
        return $tutorial;
    }
    
    public function set_published(){
        $this->db->where('pubdate <=', date('Y-m-d'));
    }
    
    public function get_titles_of_set($arr) {

        $q = $this->db->query('select title, slug, modified from '.  $this->_table_name.' where `set` = ? and `pubdate` <= ? order by id;', 
                array($arr, date('Y-m-d')));
        return $q->result();
    }
    
    public function get_count($tag) {
        
        $q = $this->db->query('select count(id) as c from '. 
                $this->_table_name.' where `tags` like ? and `pubdate` <= ?;', 
                array($tag, date('Y-m-d')));
        return $q->row();
    }
    
    public function get_count_set($set) {
        
        $q = $this->db->query('select count(id) as c from '. 
                $this->_table_name.' where `set` like ? and `pubdate` <= ?;', 
                array($set, date('Y-m-d')));
        return $q->row();
    }
    
    public function get_by_tag($where) {
//        $q = $this->db->query('select (`id`, `title`, `set`, `tags`, `slug`, `category`, `body`, `pubdate`) from '. 
//                $this->_table_name.' where `tags` like ? and `pubdate` <= ? order by id;', 
//                array($where, date('Y-m-d')));
        $q = $this->db->query('select * from ' . 
                $this->_table_name . ' where `tags` like ? and `pubdate` <= ? order by id;', 
                array($where, date('Y-m-d')));
        return $q->result();
    }
    
    public function get_next($arr) {

        $q = $this->db->query('select id, slug from ' .  $this->_table_name.' where `id` > ? and `category` = ? and `pubdate` <= ? order by id limit 1;', 
                array($arr['id'], $arr['category'], date('Y-m-d')));
        return $q->row();
    }
    
    public function get_prev($arr) {

        $q = $this->db->query('select id, slug from ' .  $this->_table_name.' where `id` < ? and `category` = ? and `pubdate` <= ? order by id limit 1;', 
                array($arr['id'], $arr['category'], date('Y-m-d')));
        return $q->row();
    }    
}
