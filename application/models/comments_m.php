<?php

class Comments_m extends My_Model {
    
    protected $_table_name = 'comments';
    protected $_order_by = 'reply desc';
    protected $_timestamp = FALSE;
    public $rules = array(
        'pubdate' => array('field' => 'pubdate',
                         'label' => 'Publication Date',
                         'rules' => 'trim|xss_clean'),
        'nick' => array('field' => 'nick',
                         'label' => 'nick',
                         'rules' => 'trim|max_length[100]|xss_clean'),
        'slug' => array('field' => 'slug',
                         'label' => 'Slug',
                         'rules' => 'trim|max_length[100]|url_title|xss_clean'),
        'page' => array('field' => 'page',
                         'label' => 'Page',
                         'rules' => 'trim|max_length[100]|url_title|xss_clean'),
        'comment' => array('field' => 'comment',
                         'label' => 'Comment',
                         'rules' => 'trim|required|xss_clean'),
    );

    function unique() {
        return TRUE;
    }
    
    public function get_new() {
        $comment = new stdClass();
        $comment->nick = '';
        $comment->user_id = '';
        $comment->slug = '';
        $comment->page = '';
        $comment->comment = '';
        $comment->pubdate = '';
        return $comment;
    }

    public function get_id() {
        $this->db->select('comments.id');
        $this->db->limit(1);
        $this->db->order_by('id desc');
        $query = $this->db->get('comments');
        return $query->row();
    }
    
    public function get_recent($limit = 3) {

        // Fetch a limited number of recent comments
        $limit = (int) $limit;
        $this->db->limit($limit);
        return parent::get();
    }
    public function get_comments($page='blog', $slug=NULL) {
        $this->db->select('comments.*, frontend_users.image, frontend_users.bio');
        $this->db->where('slug',$slug);
        $this->db->where('page',$page);
        $this->db->order_by('comment_id, reply, pubdate, id');
        
        $this->db->join('frontend_users', 'comments.nick = frontend_users.nick');
        $query = $this->db->get('comments');
        $result = $query->result_array();
        return $result;
    }
}
