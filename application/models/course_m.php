<?php 
class Course_m extends My_Model {
    
    protected $_table_name = 'Courses';
    protected $_order_by = 'pubdate desc, id desc';
    protected $_timestamp = TRUE;
    public $rules = array(
        'pubdate' => array('field' => 'pubdate',
                         'label' => 'Publication Date',
                         'rules' => 'trim|required|exact_length[10]|xss_clean'),
        'title' => array('field' => 'title',
                         'label' => 'Title',
                         'rules' => 'trim|required|max_length[100]|xss_clean'),
        'slug' => array('field' => 'slug',
                         'label' => 'Slug',
                         'rules' => 'trim|required|max_length[100]|url_title|xss_clean'),
        'tags' => array('field' => 'tags',
                         'label' => 'Tags',
                         'rules' => 'trim|required|xss_clean'),
        'syllabus' => array('field' => 'syllabus',
                         'label' => 'Syllabus',
                         'rules' => 'trim|required|xss_clean'),
        'image' => array('field' => 'image',
                         'label' => 'Image',
                         'rules' => 'trim|required|xss_clean'),
        'instructor' => array('field' => 'instructor',
                         'label' => 'Instructor',
                         'rules' => 'trim|required|xss_clean'),
        'time' => array('field' => 'time',
                         'label' => 'Time',
                         'rules' => 'trim|required|max_length[100]|xss_clean'),
        'links' => array('field' => 'links',
                         'label' => 'Links',
                         'rules' => 'trim|required|xss_clean'),
        'summary' => array('field' => 'summary',
                         'label' => 'summary',
                         'rules' => 'trim|required|xss_clean'),
    );

    public function get_new() {
        $course = new stdClass();
        $course->title = '';
        $course->slug = '';
        $course->tags = '';
        $course->instructor = '';
        $course->time = '';
        $course->image = '';
        $course->syllabus = '';
        $course->links = '';
        $course->summary = '';
        $course->pubdate = date('Y-m-d');
        return $course;
    }
    
    public function set_published(){
        $this->db->where('pubdate <=', date('Y-m-d'));
    }

    public function get_recent($limit = 3){

        // Fetch a limited number of recent courses
        $limit = (int) $limit;
        $this->set_published();
        $this->db->limit($limit);
        return parent::get();
    }
    
}

