<?php
class Frontend_user_m extends My_Model
{
    
    protected $_table_name = 'frontend_users';
    protected $_order_by = 'name';
    public $rules = array(
        'email' => array('field' => 'email',
                         'label' => 'Email',
                         'rules' => 'trim|required|valid_email|xss_clean'),
        'password' => array('field' => 'password',
                            'label' => 'Password',
                            'rules' => 'trim|required')
    );
    public $rules_user = array(
        'email' => array('field' => 'email',
                         'label' => 'Email',
                         'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'),
        'name' => array('field' => 'name',
                         'label' => 'Name',
                         'rules' => 'trim|required|xss_clean'),
        'nick' => array('field' => 'nick',
                         'label' => 'Nick',
                         'rules' => 'trim|required|xss_clean|is_unique[frontend_users.nick]'),
        'password' => array('field' => 'password',
                            'label' => 'Password',
                            'rules' => 'trim|matches[password_confirm]'),
    );
    protected $_timestamp = FALSE;
    
    public function __construct() {
        parent::__construct();
    }
    
    
    
    public function login() {
        $user = $this->get_by(array('email' => $this->input->post('email'),
            'password' => $this->hash($this->input->post('password'))), TRUE);
        if (count($user)) {
            // Log in user
            $data = array('name' => $user->name,
                'email' => $user->email,
                'id' => $user->id,
                'nick' => $user->nick,
                'image' => $user->image,
                'bio' => $user->bio,
                'points' => $user->points,
                'loggedin' => TRUE,
                'type' => 1
            );
            $this->session->set_userdata($data);
            return TRUE;
        }
        else {
            return FALSE;
        }
    }
    
    public function getDetails($email) {
        $user = $this->get_by(array('email' => $email), TRUE);
        if (count($user)) {
            return $user;
        }
        return NULL;
    }
    
    public function get_nick() {
        $user = $this->get_by(array('email' => $this->input->post('email'),
            'password' => $this->hash($this->input->post('password'))), TRUE);
        if (count($user)) {
            return $user->nick;
        }
        return NULL;
    }
    
    public function setUserData($data) {
        if(isset($data) && count($data)) {
            $user = $this->get_by(array('email' => $data['email']), TRUE);
            if (count($user)) {
                $data['nick'] = $user->nick;
                $data['id'] = $user->id;
                $data['points'] = $user->points;
            }
            $this->session->set_userdata($data);
        }
    }
    
    public function logout() {
        $this->session->sess_destroy();
    }
    
    public function hash($string) {
        return hash('sha512', $string . config_item('encrption_key'));
    }
    
    public function get_new() {
        $user = new stdClass();
        $user->name = '';
        $user->nick = '';
        $user->email = '';
        $user->password = '';
        $user->image = 'img/default.png';
        $user->verify = 0;
        $user->points = 0;
        $user->bio = NULL;
        $user->type = 1;
        return $user;
    }
    
    public function edit_details($data) {
            $this->db->set($data);
            $this->db->where($this->_primary_key, $this->session->userdata('id'));
            $this->db->update($this->_table_name);
            $data1 = array('name' => $data['name'],
                'email' => $this->session->userdata('email'),
                'id' => $this->session->userdata('id'),
                'nick' => $data['nick'],
                'loggedin' => TRUE,
                'type' => 1);
            $this->session->set_userdata($data1);
    }
    
}
 