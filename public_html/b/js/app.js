function cnf(v,lan) {
    v.setTheme("ace/theme/textmate");
    v.getSession().setMode("ace/mode/"+lan);
    v.setReadOnly(true);
    v.setOptions({maxLines: 100});

}