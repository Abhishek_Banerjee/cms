-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2014 at 05:07 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `ace_tags`
--

CREATE TABLE IF NOT EXISTS `ace_tags` (
  `id` int(11) unsigned NOT NULL,
  `page` varchar(20) NOT NULL,
  `tag` varchar(200) NOT NULL,
  `ace_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ace_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ace_tags`
--

INSERT INTO `ace_tags` (`id`, `page`, `tag`, `ace_id`) VALUES
(8, 'tutorial', 'python-block-1', 1),
(10, 'blog', 'python-block-1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `pubdate` date NOT NULL,
  `body` text NOT NULL,
  `tags` varchar(500) DEFAULT NULL,
  `modified` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `image` varchar(150) NOT NULL,
  `votes` int(20) NOT NULL,
  `total_stars` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `slug`, `pubdate`, `body`, `tags`, `modified`, `user_id`, `image`, `votes`, `total_stars`) VALUES
(1, 'My First Page', 'My_First_Page', '2014-02-09', 'My First Page', '', 1396501799, 0, '', 0, 0),
(3, 'guiiu', 'guiiu', '2014-12-09', 'guiiu', '', 1396501799, 0, '', 0, 0),
(4, 'My First Page hcj', 'my-first-page-o', '2014-02-13', 'ftyvkvvvvvvvvvv\r\ngn\r\nb\r\nb\r\nxb\r\nxbx\r\nbxxxxxxxxxxxxxx\r\nb\r\nbv\r\nbv \r\n\r\nbv\r\nbngbng\r\nbgh\r\nbxn', '', 1396501799, 0, '', 0, 0),
(5, 'Temp', 'temp', '2014-02-13', 'hjv hgkvkh j vvkhgFigure 10.5: The linked list of Figure 10.3(a) represented by the arrays key, next, and prev.\r\nEach vertical slice of the arrays represents a single object. Stored pointers correspond to the\r\narray indices shown at the top; the arrows show how to interpret them. Lightly shaded object\r\npositions contain list elements. The variable L keeps the index of the Head.\r\nIn Figure 10.3(a), the object with key 4 follows the object with key 16 in the linked list. In\r\nFigure 10.5, key 4 appears in key[2], and key 16 appears in key[5], so we have next[5] = 2 and\r\nprev[2] = 5. Although the constant NIL appears in the next field of the tail and the prev field\r\nof the head, we usually use an integer (such as 0 or -1) that cannot possibly represent an actual\r\nindex into the arrays. A variable L holds the index of the head of the list.\r\nIn our pseudocode, we have been using square brackets to denote both the indexing of an\r\narray and the selection of a field (attribute) of an object. Either way, the meanings of key[x],\r\nnext[x], and prev[x] are consistent with implementation practice.\r\nA single-array representation of objects\r\nThe words in a computer memory are typically addressed by integers from 0 to M - 1, where\r\nM is a suitably large integer. In many programming languages, an object occupies a\r\ncontiguous set of locations in the computer memory. A pointer is simply the address of the\r\nfirst memory location of the object, and other memory locations within the object can be\r\nindexed by adding an offset to the pointer.\r\nWe can use the same strategy for implementing objects in programming environments that do\r\nnot provide explicit pointer data types. For example, Figure 10.6 shows how a single array A\r\ncan be used to store the linked list from Figures 10.3(a) and 10.5. An object occupies a\r\ncontiguous subarray A[j  k]. Each field of the object corresponds to an offset in the range\r\nfrom 0 to k - j, and a pointer to the object is the index j. In Figure 10.6, the offsets\r\ncorresponding to key, next, and prev are 0, 1, and 2, respectively. To read the value of prev[i],\r\ngiven a pointer i, we add the value i of the pointer to the offset 2, thus reading A[i + 2].\r\nFigure 10.6: The linked list of Figures 10.3(a) and 10.5 represented in a single array A. Each\r\nlist element is an object that occupies a contiguous subarray of length 3 within the array. The\r\nthree fields key, next, and prev correspond to the offsets 0, 1, and 2, respectively. A pointer to\r\nan object is an index of the first element of the object. Objects containing list elements are\r\nlightly shaded, and arrows show the list ordering.', '', 1396501799, 0, '', 0, 0),
(6, 'guiiu', 'temp-uuu', '2014-02-13', 'Figure 10.5: The linked list of Figure 10.3(a) represented by the arrays key, next, and prev.\r\nEach vertical slice of the arrays represents a single object. Stored pointers correspond to the\r\narray indices shown at the top; the arrows show how to interpret them. Lightly shaded object\r\npositions contain list elements. The variable L keeps the index of the Head.\r\nIn Figure 10.3(a), the object with key 4 follows the object with key 16 in the linked list. In\r\nFigure 10.5, key 4 appears in key[2], and key 16 appears in key[5], so we have next[5] = 2 and\r\nprev[2] = 5. Although the constant NIL appears in the next field of the tail and the prev field\r\nof the head, we usually use an integer (such as 0 or -1) that cannot possibly represent an actual\r\nindex into the arrays. A variable L holds the index of the head of the list.\r\nIn our pseudocode, we have been using square brackets to denote both the indexing of an\r\narray and the selection of a field (attribute) of an object. Either way, the meanings of key[x],\r\nnext[x], and prev[x] are consistent with implementation practice.\r\nA single-array representation of objects\r\nThe words in a computer memory are typically addressed by integers from 0 to M - 1, where\r\nM is a suitably large integer. In many programming languages, an object occupies a\r\ncontiguous set of locations in the computer memory. A pointer is simply the address of the\r\nfirst memory location of the object, and other memory locations within the object can be\r\nindexed by adding an offset to the pointer.\r\nWe can use the same strategy for implementing objects in programming environments that do\r\nnot provide explicit pointer data types. For example, Figure 10.6 shows how a single array A\r\ncan be used to store the linked list from Figures 10.3(a) and 10.5. An object occupies a\r\ncontiguous subarray A[j  k]. Each field of the object corresponds to an offset in the range\r\nfrom 0 to k - j, and a pointer to the object is the index j. In Figure 10.6, the offsets\r\ncorresponding to key, next, and prev are 0, 1, and 2, respectively. To read the value of prev[i],\r\ngiven a pointer i, we add the value i of the pointer to the offset 2, thus reading A[i + 2].\r\nFigure 10.6: The linked list of Figures 10.3(a) and 10.5 represented in a single array A. Each\r\nlist element is an object that occupies a contiguous subarray of length 3 within the array. The\r\nthree fields key, next, and prev correspond to the offsets 0, 1, and 2, respectively. A pointer to\r\nan object is an index of the first element of the object. Objects containing list elements are\r\nlightly shaded, and arrows show the list ordering.', '', 1396501799, 10, '', 0, 0),
(7, 'My First Page jhvjvjvj', 'ughuhg909', '2014-02-13', 'Figure 10.5: The linked list of Figure 10.3(a) represented by the arrays key, next, and prev.\r\nEach vertical slice of the arrays represents a single object. Stored pointers correspond to the\r\narray indices shown at the top; the arrows show how to interpret them. Lightly shaded object\r\npositions contain list elements. The variable L keeps the index of the Head.\r\nIn Figure 10.3(a), the object with key 4 follows the object with key 16 in the linked list. In\r\nFigure 10.5, key 4 appears in key[2], and key 16 appears in key[5], so we have next[5] = 2 and\r\nprev[2] = 5. Although the constant NIL appears in the next field of the tail and the prev field\r\nof the head, we usually use an integer (such as 0 or -1) that cannot possibly represent an actual\r\nindex into the arrays. A variable L holds the index of the head of the list.\r\nIn our pseudocode, we have been using square brackets to denote both the indexing of an\r\narray and the selection of a field (attribute) of an object. Either way, the meanings of key[x],\r\nnext[x], and prev[x] are consistent with implementation practice.\r\nA single-array representation of objects\r\nThe words in a computer memory are typically addressed by integers from 0 to M - 1, where\r\nM is a suitably large integer. In many programming languages, an object occupies a\r\ncontiguous set of locations in the computer memory. A pointer is simply the address of the\r\nfirst memory location of the object, and other memory locations within the object can be\r\nindexed by adding an offset to the pointer.\r\nWe can use the same strategy for implementing objects in programming environments that do\r\nnot provide explicit pointer data types. For example, Figure 10.6 shows how a single array A\r\ncan be used to store the linked list from Figures 10.3(a) and 10.5. An object occupies a\r\ncontiguous subarray A[j  k]. Each field of the object corresponds to an offset in the range\r\nfrom 0 to k - j, and a pointer to the object is the index j. In Figure 10.6, the offsets\r\ncorresponding to key, next, and prev are 0, 1, and 2, respectively. To read the value of prev[i],\r\ngiven a pointer i, we add the value i of the pointer to the offset 2, thus reading A[i + 2].\r\nFigure 10.6: The linked list of Figures 10.3(a) and 10.5 represented in a single array A. Each\r\nlist element is an object that occupies a contiguous subarray of length 3 within the array. The\r\nthree fields key, next, and prev correspond to the offsets 0, 1, and 2, respectively. A pointer to\r\nan object is an index of the first element of the object. Objects containing list elements are\r\nlightly shaded, and arrows show the list ordering.', '', 1396501799, 0, '', 0, 0),
(8, 'My First Page jhvjvjvjbubvbjuvu', 'huh98899', '2014-02-13', 'Figure 10.5: The linked list of Figure 10.3(a) represented by the arrays key, next, and prev.\r\nEach vertical slice of the arrays represents a single object. Stored pointers correspond to the\r\narray indices shown at the top; the arrows show how to interpret them. Lightly shaded object\r\npositions contain list elements. The variable L keeps the index of the Head.\r\nIn Figure 10.3(a), the object with key 4 follows the object with key 16 in the linked list. In\r\nFigure 10.5, key 4 appears in key[2], and key 16 appears in key[5], so we have next[5] = 2 and\r\nprev[2] = 5. Although the constant NIL appears in the next field of the tail and the prev field\r\nof the head, we usually use an integer (such as 0 or -1) that cannot possibly represent an actual\r\nindex into the arrays. A variable L holds the index of the head of the list.\r\nIn our pseudocode, we have been using square brackets to denote both the indexing of an\r\narray and the selection of a field (attribute) of an object. Either way, the meanings of key[x],\r\nnext[x], and prev[x] are consistent with implementation practice.\r\nA single-array representation of objects\r\nThe words in a computer memory are typically addressed by integers from 0 to M - 1, where\r\nM is a suitably large integer. In many programming languages, an object occupies a\r\ncontiguous set of locations in the computer memory. A pointer is simply the address of the\r\nfirst memory location of the object, and other memory locations within the object can be\r\nindexed by adding an offset to the pointer.\r\nWe can use the same strategy for implementing objects in programming environments that do\r\nnot provide explicit pointer data types. For example, Figure 10.6 shows how a single array A\r\ncan be used to store the linked list from Figures 10.3(a) and 10.5. An object occupies a\r\ncontiguous subarray A[j  k]. Each field of the object corresponds to an offset in the range\r\nfrom 0 to k - j, and a pointer to the object is the index j. In Figure 10.6, the offsets\r\ncorresponding to key, next, and prev are 0, 1, and 2, respectively. To read the value of prev[i],\r\ngiven a pointer i, we add the value i of the pointer to the offset 2, thus reading A[i + 2].\r\nFigure 10.6: The linked list of Figures 10.3(a) and 10.5 represented in a single array A. Each\r\nlist element is an object that occupies a contiguous subarray of length 3 within the array. The\r\nthree fields key, next, and prev correspond to the offsets 0, 1, and 2, respectively. A pointer to\r\nan object is an index of the first element of the object. Objects containing list elements are\r\nlightly shaded, and arrows show the list ordering.', '', 1396501799, 0, '', 0, 0),
(9, 'new article', 'new-article', '2014-02-13', 'jkbd  bdhjbdbhj fjbjbdj dsbckjb\r\nNow that we have some idea about how our site is supposed to look and function fresh in our minds\r\nfrom the last chapter, we’re going to get started by creating the basic skeleton for the site.\r\nFirst, we’re going to create the Django site and have a look at the files that Django creates for you by\r\ndefault, and how you’re supposed to use each one to set up the site. As every dynamic web site requires a\r\nback-end datastore, we’re going to create and configure the MySQL database for our project. Then, we’ll\r\nlearn how Django maps requests to your site to view functions, and how these serve templates back to\r\nthe users of your site.\r\nWe’re just going to create something very simple, but don’t let this interfere with your own design\r\nneeds. You’re welcome to detract from my setup and use your own custom layout inside of our template\r\nfiles. You should be able to follow along with the technical concepts in the rest of the book using your\r\nown HTML and CSS.\r\nA Django-istic Welcome\r\nSo, now that we have our development environment up and running, let’s create our first Django site!\r\nActually, when compared to getting Django installed and running properly, creating the site and getting\r\nto our first default “Welcome!” page is really a breeze. This is probably a good thing: after spending the\r\nfirst chapter plowing through our schemes and configuring all the software we’re going to be using, I’m\r\nsure you’re ready for some instant gratification.\r\nCreating the Project\r\nThe first thing to do, whether or not you’ve opted to use the Eclipse IDE, is to fire up a shell on your\r\nsystem and navigate to the directory where you plan to store your project. On my system, this happens\r\nto be in /home/smoochy/eclipse/workspace. Once you’re there, run the following command:\r\n$ django-admin.py startproject ecomstore\r\nOnce you’ve run this, go ahead and run ls to see that a directory called ecomstore was, in fact,\r\ncreated for you. If it’s there, change into it and boot up your new site by doing the following:\r\n$ cd ecomstore\r\n$ python manage.py runserver\r\nYou should see a few quick lines about validating Django’s models, which I’ll discuss in later\r\nchapters. More importantly, you should see a URL where you can view your new site in the browser.', '', 1396501799, 0, '', 2, 2),
(10, 'My First Page 2', 'blog4', '2014-04-03', 'ouldb dbjnkckjc bjk dcb<div class="well" id="python-block-1">def p():\n    w = int(''3'')\n    print w</div>', '#java#basic#a#s#s#DD#', 1396501799, 0, '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('5e26102297b398e843837d64044167bd', '::1', 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36', 1397572630, '');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(100) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `pubdate` int(20) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `page` varchar(50) NOT NULL,
  `user_id` int(20) NOT NULL,
  `reply` int(2) NOT NULL DEFAULT '0',
  `comment_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `nick`, `comment`, `pubdate`, `slug`, `page`, `user_id`, `reply`, `comment_id`) VALUES
(1, 'ashish', 'solomon', 1396766486, 'new-article', 'blog', 6, 0, 1),
(2, 'ashish', 'grundy', 1396766557, 'new-article', 'blog', 6, 1, 1),
(4, 'ashish', 'born', 1396766740, 'new-article', 'blog', 6, 0, 2),
(5, '0', 'ln', 1396788468, 'new-article', 'blog', 0, 1, 1),
(6, 'ashish', 'jh', 1396870319, 'new-article', 'blog', 6, 1, 1),
(7, 'ashish', 'yjfsaj', 1396870461, 'new-article', 'blog', 6, 0, 6),
(8, 'ashish', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1396967401, 'new-article', 'blog', 6, 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `instructor` varchar(50) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `tags` varchar(100) NOT NULL,
  `syllabus` varchar(100) NOT NULL,
  `pubdate` date NOT NULL,
  `links` varchar(1000) NOT NULL,
  `time` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `summary` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `image`, `instructor`, `slug`, `tags`, `syllabus`, `pubdate`, `links`, `time`, `created`, `modified`, `summary`) VALUES
(1, 'beginning java', 'beginning_java/img.png', 'Abhishek', 'beginning-java', 'java basic', 'beginning_java/syllabus', '2014-02-17', '1.mp4 2.mp4', '5m00s 10m20s', '2014-02-17 10:46:29', '2014-02-17 10:46:29', 'Best of all, you now have a simple, reusable solution for a common web-development\r\ntask: a brochureware-style CMS. Any time you need to re-create it, you can set up Django\r\nand walk through these same easy steps (or even just make a copy of the project, changing\r\nthe appropriate settings in the process). Doing this will save you time and free you from the\r\ntedium of a fairly repetitive situation.');

-- --------------------------------------------------------

--
-- Table structure for table `frontend_users`
--

CREATE TABLE IF NOT EXISTS `frontend_users` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `nick` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `verify` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(150) NOT NULL DEFAULT 'img/default.png',
  `bio` varchar(50) DEFAULT NULL,
  `points` int(30) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `nick` (`nick`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `frontend_users`
--

INSERT INTO `frontend_users` (`id`, `email`, `password`, `nick`, `name`, `verify`, `image`, `bio`, `points`) VALUES
(0, 'annonymous@som.com', '@fdskjfdjbfdjfjvnkjckjc ', 'Anonymous', 'Anonymous', 1, 'img/ann.png', NULL, 0),
(5, 'e297849@drdrb.com', '2e96772232487fb3a058d58f2c310023e07e4017c94d56cc5fae4b54b44605f42a75b0b1f358991f8c6cbe9b68b64e5b2a09d0ad23fcac07ee9a9198a745e1d5', 'jbkjl', 'abhi1992', 0, 'img/default.png', NULL, 0),
(6, 'imrealashu@gmail.com', 'e54ee7e285fbb0275279143abc4c554e5314e7b417ecac83a5984a964facbaad68866a2841c3e83ddf125a2985566261c4014f9f960ec60253aebcda9513a9b4', 'ashish', 'ashish', 1, 'img/default2.png', 'Ashish', 0),
(7, 'e2978j49@drdrb.com', '7b0776dbac74a9abb8a0d0119c73ae82efbb1b95af009f0669e2b52c9664f7fbb458ddf3abd7ccf0617ab69362a13ade046c4f19490710e5d8937ba72efa90aa', 'kbj', 'bh', 0, 'img/default.png', NULL, 0),
(8, 'e29784w9@drdrb.com', '7e7246cbaa79f710fe57ba338f684064a416198270db19b4a0c509c83755a9a307c87b21736c99a7dbff5da96a6191ec89ce9fbace919a6d8455f51d78e0313f', 'jbwkjl', 'www', 0, 'img/default.png', NULL, 0),
(9, 'e2978f49@drdrb.com', 'acc28db2beb7b42baa1cb0243d401ccb4e3fce44d7b02879a52799aadff541522d8822598b2fa664f9d5156c00c924805d75c3868bd56c2acb81d37e98e35adc', 'abh', 'cbv', 0, 'img/default.png', NULL, 0),
(10, 'abhishekbanerjee1992@gmail.com', '3bf33de1c9c595dc14c4c0f41989bd3b45af4e0280fe062ef8350f88a91f503b16c53fefc07196bf34768b0a1167892e19c58ea7b19882333504616ac0e7294a', 'abhishek', 'Abhishek Banerjee', 0, 'img/default7.png', 'qwertyuioplkjhgfdsazxcvbnmqwertyuiopasdfghjklzxcvb', 0),
(11, 'imrealawshu@gmail.com', 'aa66509891ad28030349ba9581e8c92528faab6a34349061a44b6f8fcd8d6877a67b05508983f12f8610302d1783401a07ec41c7e9ebd656de34ec60d84d9511', 'ew', 'ew', 0, 'img/default.png', NULL, 0),
(17, 'abhishekbanklnerjee1992@gmail.com', '546c1d33718fab99c564b46c7301107c74d01dfcdf52f88350d10718a5ed1d3623f615d953db9f7f9f8471db0eaa91b51d7c7a3dc931e7782afc2ba28b85b603', 'y', 'ty', 0, 'img/default.png', NULL, 0),
(19, 'abhishekbakbnerjee1992@gmail.com', '546c1d33718fab99c564b46c7301107c74d01dfcdf52f88350d10718a5ed1d3623f615d953db9f7f9f8471db0eaa91b51d7c7a3dc931e7782afc2ba28b85b603', 'kijhu', 'lkn', 0, 'img/default.png', NULL, 0),
(20, 'abhishekbanerjee1bjk992@gmail.com', 'cded74740d4bbfd4eb126d6de454b59e2d631f36c0ae0d2325b5e2be4da2befe792106b98422ad24c092e8f184b2ff4be0bbbcdcd278db6c2427533b2e9264d1', 'nl', 'b,j', 0, 'img/default.png', NULL, 0),
(21, 'imrealabjkshu@gmail.com', '546c1d33718fab99c564b46c7301107c74d01dfcdf52f88350d10718a5ed1d3623f615d953db9f7f9f8471db0eaa91b51d7c7a3dc931e7782afc2ba28b85b603', 'kgu', 'jk', 0, 'img/default.png', NULL, 0),
(24, 'ooabhi1992oo@gmail.com', 'f1d32df27c9a71d3f83fd779ee2072e198aecfa76f3aa89d40cd97207b1342f0d42b671f889c3ce82e2006715c03e29ed76c119bca6b84f185e321ffdfe2a99e', 'Abhishek_Banerjee', 'Abhishek Banerjee', 0, 'https://lh3.googleusercontent.com/-c5rhMD0Q_zM/AAAAAAAAAAI/AAAAAAAAABE/61k_qOQzZeI/photo.jpg', NULL, 0),
(25, 'a@gmail.com', '0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8', 'A', 'Abhishek Banerjee', 0, 'img/default.png', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(12);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `body` text NOT NULL,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `template` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `order`, `body`, `parent_id`, `template`) VALUES
(1, 'Homepage', '', 1, 'yvfytvytvkgchgcyt', 0, 'homepage'),
(4, 'Contact', 'contact', 0, 'contact', 0, 'page'),
(5, 'Blog', 'blog', 0, 'blog', 0, 'blog'),
(6, 'Tutorials', 'tutorial', 0, 'tutorial', 0, 'tutorial'),
(7, 'Course', 'course', 0, 'course', 0, 'course_archive');

-- --------------------------------------------------------

--
-- Table structure for table `stars`
--

CREATE TABLE IF NOT EXISTS `stars` (
  `page_id` int(20) NOT NULL,
  `page` varchar(50) NOT NULL,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stars`
--

INSERT INTO `stars` (`page_id`, `page`, `user_id`) VALUES
(9, 'blog', 21);

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE IF NOT EXISTS `tutorials` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `slug` varchar(500) NOT NULL,
  `tags` varchar(500) NOT NULL,
  `set` varchar(300) NOT NULL,
  `category` varchar(100) NOT NULL,
  `pubdate` date NOT NULL,
  `body` text NOT NULL,
  `modified` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `image` varchar(150) NOT NULL,
  `votes` mediumtext,
  `total_stars` int(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tutorials`
--

INSERT INTO `tutorials` (`id`, `title`, `slug`, `tags`, `set`, `category`, `pubdate`, `body`, `modified`, `user_id`, `image`, `votes`, `total_stars`) VALUES
(1, 'java Tutorial 1', 'java-tut-1', '#java#basic#', 'java-basics', 'java', '2014-02-16', 'Each high-level language has its own syntax, or rules of the language. For example,\ndepending on the specific high-level language, you might use the verb “print” or “write” to\nproduce output. All languages have a specific, limited vocabulary and a specific set of rules\nfor using that vocabulary. When you are learning a computer programming language, such\nas Java, C++, or Visual Basic, you really are learning the vocabulary and syntax rules for\nthat language.\nUsing a programming language, programmers write a series of program statements, similar\nto English sentences, to carry out the tasks they want the program to perform. After the\nprogram statements are written, high-level language programmers use a computer program\ncalled a compiler or interpreter to translate their language statements into machine\ncode. A compiler translates an entire program before carrying out the statement, or\nexecuting it, whereas an interpreter translates one program statement at a time, executing\na statement as soon as it is translated. Compilers and interpreters issue one or more\nerror messages each time they encounter an invalid program statement—that is, a statement\ncontaining a syntax error, or misuse of the language. Subsequently, the programmer\ncan correct the error and attempt another translation by compiling or interpreting the program\nagain. Locating and repairing all syntax errors is part of the process of debugging a\nprogram—freeing the program of all errors. Whether you use a compiler or interpreter often\ndepends on the programming language you use—for example, C++ is a compiled language\nand Visual Basic is an interpreted language. Each type of translator has its supporters—\nprograms written in compiled languages execute more quickly, whereas programs written in\ninterpreted languages are easier to develop and debug. Java uses the best of both technologies—\na compiler to translate your programming statements and an interpreter to read the compiled\ncode line by line at run time.\nIn addition to learning the correct syntax for a particular language, a programmer also must\nunderstand computer programming logic. The logic behind any program involves executing\nthe various statements and procedures in the correct order to produce the desired results.\nAlthough you begin to debug a program by correcting all the syntax errors, it is not fully\ndebugged until you have also fixed all logical errors. For example, you would not write statements\nto tell the computer program to process data until the data had been properly read into\nthe program. Similarly, you might be able to use a computer language’s syntax correctly, but\nfail to end up with a logically constructed, workable program. Examples of logical errors\ninclude multiplying two values when you meant to divide them, or producing output prior to\nobtaining the appropriate input. Tools that will help you visualize and understand logic are\npresented in Chapter 5.', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(2, 'java Tutorial 2', 'java-tut-2', '#java#basic#', 'java-basics', 'java', '2014-02-16', 'Each high-level language has its own syntax, or rules of the language. For example,\r\ndepending on the specific high-level language, you might use the verb â€œprintâ€ or â€œwriteâ€ to\r\nproduce output. All languages have a specific, limited vocabulary and a specific set of rules\r\nfor using that vocabulary. When you are learning a computer programming language, such\r\nas Java, C++, or Visual Basic, you really are learning the vocabulary and syntax rules for\r\nthat language.\r\nUsing a programming language, programmers write a series of program statements, similar\r\nto English sentences, to carry out the tasks they want the program to perform. After the\r\nprogram statements are written, high-level language programmers use a computer program\r\ncalled a compiler or interpreter to translate their language statements into machine\r\ncode. A compiler translates an entire program before carrying out the statement, or\r\nexecuting it, whereas an interpreter translates one program statement at a time, executing\r\na statement as soon as it is translated. Compilers and interpreters issue one or more\r\nerror messages each time they encounter an invalid program statementâ€”that is, a statement\r\ncontaining a syntax error, or misuse of the language. Subsequently, the programmer\r\ncan correct the error and attempt another translation by compiling or interpreting the program\r\nagain. Locating and repairing all syntax errors is part of the process of debugging a\r\nprogramâ€”freeing the program of all errors. Whether you use a compiler or interpreter often\r\ndepends on the programming language you useâ€”for example, C++ is a compiled language\r\nand Visual Basic is an interpreted language. Each type of translator has its supportersâ€”\r\nprograms written in compiled languages execute more quickly, whereas programs written in\r\ninterpreted languages are easier to develop and debug. Java uses the best of both technologiesâ€”\r\na compiler to translate your programming statements and an interpreter to read the compiled\r\ncode line by line at run time.\r\nIn addition to learning the correct syntax for a particular language, a programmer also must\r\nunderstand computer programming logic. The logic behind any program involves executing\r\nthe various statements and procedures in the correct order to produce the desired results.\r\nAlthough you begin to debug a program by correcting all the syntax errors, it is not fully\r\ndebugged until you have also fixed all logical errors. For example, you would not write statements\r\nto tell the computer program to process data until the data had been properly read into\r\nthe program. Similarly, you might be able to use a computer languageâ€™s syntax correctly, but\r\nfail to end up with a logically constructed, workable program. Examples of logical errors\r\ninclude multiplying two values when you meant to divide them, or producing output prior to\r\nobtaining the appropriate input. Tools that will help you visualize and understand logic are\r\npresented in Chapter 5.', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(3, 'java Tutorial 3', 'java-tut-3', '#java#basic#', 'java-basics', 'java', '2014-02-16', 'Each high-level language has its own syntax, or rules of the language. For example,\r\ndepending on the specific high-level language, you might use the verb â€œprintâ€ or â€œwriteâ€ to\r\nproduce output. All languages have a specific, limited vocabulary and a specific set of rules\r\nfor using that vocabulary. When you are learning a computer programming language, such\r\nas Java, C++, or Visual Basic, you really are learning the vocabulary and syntax rules for\r\nthat language.\r\nUsing a programming language, programmers write a series of program statements, similar\r\nto English sentences, to carry out the tasks they want the program to perform. After the\r\nprogram statements are written, high-level language programmers use a computer program\r\ncalled a compiler or interpreter to translate their language statements into machine\r\ncode. A compiler translates an entire program before carrying out the statement, or\r\nexecuting it, whereas an interpreter translates one program statement at a time, executing\r\na statement as soon as it is translated. Compilers and interpreters issue one or more\r\nerror messages each time they encounter an invalid program statementâ€”that is, a statement\r\ncontaining a syntax error, or misuse of the language. Subsequently, the programmer\r\ncan correct the error and attempt another translation by compiling or interpreting the program\r\nagain. Locating and repairing all syntax errors is part of the process of debugging a\r\nprogramâ€”freeing the program of all errors. Whether you use a compiler or interpreter often\r\ndepends on the programming language you useâ€”for example, C++ is a compiled language\r\nand Visual Basic is an interpreted language. Each type of translator has its supportersâ€”\r\nprograms written in compiled languages execute more quickly, whereas programs written in\r\ninterpreted languages are easier to develop and debug. Java uses the best of both technologiesâ€”\r\na compiler to translate your programming statements and an interpreter to read the compiled\r\ncode line by line at run time.\r\nIn addition to learning the correct syntax for a particular language, a programmer also must\r\nunderstand computer programming logic. The logic behind any program involves executing\r\nthe various statements and procedures in the correct order to produce the desired results.\r\nAlthough you begin to debug a program by correcting all the syntax errors, it is not fully\r\ndebugged until you have also fixed all logical errors. For example, you would not write statements\r\nto tell the computer program to process data until the data had been properly read into\r\nthe program. Similarly, you might be able to use a computer languageâ€™s syntax correctly, but\r\nfail to end up with a logically constructed, workable program. Examples of logical errors\r\ninclude multiplying two values when you meant to divide them, or producing output prior to\r\nobtaining the appropriate input. Tools that will help you visualize and understand logic are\r\npresented in Chapter 5.', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(4, 'java Tutorial 4', 'java-tut-4', '#java#basic#', 'java-basics', 'java', '2014-02-17', 'Different Types of Entries\r\nYou also need to support entries that are marked as â€œdrafts,â€ which arenâ€™t meant to be shown\r\npublicly. This means youâ€™ll need some way of recording an entryâ€™s status. One way would be\r\nto use another BooleanField, with a name like is_draft or, perhaps is_public. Then you could\r\njust query for entries with the appropriate value, and authors could check or uncheck the box\r\nto control whether an entry shows up publicly.\r\nBut it would be better to have something that you can extend later. If thereâ€™s ever a need\r\nfor even one more possible value, the BooleanField wonâ€™t work. The ideal solution would be\r\nsome way to specify a list of choices and allow the user to select from them; then if you ever\r\nneed more choices, you can simply add them to the list. Django provides an easy way to do\r\nthis via an option called choices. Hereâ€™s how youâ€™ll implement it:\r\nSTATUS_CHOICES = (\r\n(1, ''Live''),\r\n(2, ''Draft''),\r\n)\r\nstatus = models.IntegerField(choices=STATUS_CHOICES, default=1)\r\nHere youâ€™re using IntegerField, which, as its name implies, stores a numberâ€”an integerâ€”\r\nin the database. But youâ€™ve used the choices option and defined a set of choices for it. The value\r\npassed to the choices option needs to be a list or a tuple, and each item in it also needs to be a\r\nlist or a tuple with the following two items:\r\nâ€¢ The actual value to store in the database\r\nâ€¢ A human-readable name to represent the choice\r\nYouâ€™ve also specified a default value: the value associated with the Live status, which will\r\ndenote weblog entries to be displayed live on the site.\r\nYou can use choices with any of Djangoâ€™s model field types, but generally itâ€™s most useful\r\nwith IntegerField (where you can use it to provide meaningful names for a list of numeric\r\nchoices) and CharField (where, for example, you can use it to store short abbreviations in the\r\ndatabase, but still keep track of the full words or phrases they represent).\r\nIf youâ€™ve used other programming languages that support enumerations, this is a similar\r\nconcept. In fact, you could (and probably should) make it look a little bit more similar. Edit the\r\nEntry model so that it begins like this:\r\nclass Entry(models.Model):\r\nLIVE_STATUS = 1\r\nDRAFT_STATUS = 2\r\nSTATUS_CHOICES = (\r\n(LIVE_STATUS, ''Live''),\r\n(DRAFT_STATUS, ''Draft''),\r\n)', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(5, 'java Tutorial 5', 'java-tut-5', '#java#basic#', 'java-basics', 'java', '2014-02-17', 'Different Types of Entries\r\nYou also need to support entries that are marked as â€œdrafts,â€ which arenâ€™t meant to be shown\r\npublicly. This means youâ€™ll need some way of recording an entryâ€™s status. One way would be\r\nto use another BooleanField, with a name like is_draft or, perhaps is_public. Then you could\r\njust query for entries with the appropriate value, and authors could check or uncheck the box\r\nto control whether an entry shows up publicly.\r\nBut it would be better to have something that you can extend later. If thereâ€™s ever a need\r\nfor even one more possible value, the BooleanField wonâ€™t work. The ideal solution would be\r\nsome way to specify a list of choices and allow the user to select from them; then if you ever\r\nneed more choices, you can simply add them to the list. Django provides an easy way to do\r\nthis via an option called choices. Hereâ€™s how youâ€™ll implement it:\r\nSTATUS_CHOICES = (\r\n(1, ''Live''),\r\n(2, ''Draft''),\r\n)\r\nstatus = models.IntegerField(choices=STATUS_CHOICES, default=1)\r\nHere youâ€™re using IntegerField, which, as its name implies, stores a numberâ€”an integerâ€”\r\nin the database. But youâ€™ve used the choices option and defined a set of choices for it. The value\r\npassed to the choices option needs to be a list or a tuple, and each item in it also needs to be a\r\nlist or a tuple with the following two items:\r\nâ€¢ The actual value to store in the database\r\nâ€¢ A human-readable name to represent the choice\r\nYouâ€™ve also specified a default value: the value associated with the Live status, which will\r\ndenote weblog entries to be displayed live on the site.\r\nYou can use choices with any of Djangoâ€™s model field types, but generally itâ€™s most useful\r\nwith IntegerField (where you can use it to provide meaningful names for a list of numeric\r\nchoices) and CharField (where, for example, you can use it to store short abbreviations in the\r\ndatabase, but still keep track of the full words or phrases they represent).\r\nIf youâ€™ve used other programming languages that support enumerations, this is a similar\r\nconcept. In fact, you could (and probably should) make it look a little bit more similar. Edit the\r\nEntry model so that it begins like this:\r\nclass Entry(models.Model):\r\nLIVE_STATUS = 1\r\nDRAFT_STATUS = 2\r\nSTATUS_CHOICES = (\r\n(LIVE_STATUS, ''Live''),\r\n(DRAFT_STATUS, ''Draft''),\r\n)', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(6, 'java Tutorial 6', 'java-tut-6', '#java#basic#', 'java-basics', 'java', '2014-02-17', 'Different Types of Entries\r\nYou also need to support entries that are marked as â€œdrafts,â€ which arenâ€™t meant to be shown\r\npublicly. This means youâ€™ll need some way of recording an entryâ€™s status. One way would be\r\nto use another BooleanField, with a name like is_draft or, perhaps is_public. Then you could\r\njust query for entries with the appropriate value, and authors could check or uncheck the box\r\nto control whether an entry shows up publicly.\r\nBut it would be better to have something that you can extend later. If thereâ€™s ever a need\r\nfor even one more possible value, the BooleanField wonâ€™t work. The ideal solution would be\r\nsome way to specify a list of choices and allow the user to select from them; then if you ever\r\nneed more choices, you can simply add them to the list. Django provides an easy way to do\r\nthis via an option called choices. Hereâ€™s how youâ€™ll implement it:\r\nSTATUS_CHOICES = (\r\n(1, ''Live''),\r\n(2, ''Draft''),\r\n)\r\nstatus = models.IntegerField(choices=STATUS_CHOICES, default=1)\r\nHere youâ€™re using IntegerField, which, as its name implies, stores a numberâ€”an integerâ€”\r\nin the database. But youâ€™ve used the choices option and defined a set of choices for it. The value\r\npassed to the choices option needs to be a list or a tuple, and each item in it also needs to be a\r\nlist or a tuple with the following two items:\r\nâ€¢ The actual value to store in the database\r\nâ€¢ A human-readable name to represent the choice\r\nYouâ€™ve also specified a default value: the value associated with the Live status, which will\r\ndenote weblog entries to be displayed live on the site.\r\nYou can use choices with any of Djangoâ€™s model field types, but generally itâ€™s most useful\r\nwith IntegerField (where you can use it to provide meaningful names for a list of numeric\r\nchoices) and CharField (where, for example, you can use it to store short abbreviations in the\r\ndatabase, but still keep track of the full words or phrases they represent).\r\nIf youâ€™ve used other programming languages that support enumerations, this is a similar\r\nconcept. In fact, you could (and probably should) make it look a little bit more similar. Edit the\r\nEntry model so that it begins like this:\r\nclass Entry(models.Model):\r\nLIVE_STATUS = 1\r\nDRAFT_STATUS = 2\r\nSTATUS_CHOICES = (\r\n(LIVE_STATUS, ''Live''),\r\n(DRAFT_STATUS, ''Draft''),\r\n)', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(7, 'java Tutorial 7', 'java-tut-7', '#java#basic#a#s#s#S#DD#javm m nb nbnvnvnvnvna#basic#a#s#s#S#DD#java#basic#a#s#s#S#DD#', 'java-basics', 'java', '2014-02-17', 'Different Types of Entries\r\nYou also need to support entries that are marked as â€œdrafts,â€ which arenâ€™t meant to be shown\r\npublicly. This means youâ€™ll need some way of recording an entryâ€™s status. One way would be\r\nto use another BooleanField, with a name like is_draft or, perhaps is_public. Then you could\r\njust query for entries with the appropriate value, and authors could check or uncheck the box\r\nto control whether an entry shows up publicly.\r\nBut it would be better to have something that you can extend later. If thereâ€™s ever a need\r\nfor even one more possible value, the BooleanField wonâ€™t work. The ideal solution would be\r\nsome way to specify a list of choices and allow the user to select from them; then if you ever\r\nneed more choices, you can simply add them to the list. Django provides an easy way to do\r\nthis via an option called choices. Hereâ€™s how youâ€™ll implement it:\r\nSTATUS_CHOICES = (\r\n(1, ''Live''),\r\n(2, ''Draft''),\r\n)\r\nstatus = models.IntegerField(choices=STATUS_CHOICES, default=1)\r\nHere youâ€™re using IntegerField, which, as its name implies, stores a numberâ€”an integerâ€”\r\nin the database. But youâ€™ve used the choices option and defined a set of choices for it. The value\r\npassed to the choices option needs to be a list or a tuple, and each item in it also needs to be a\r\nlist or a tuple with the following two items:\r\nâ€¢ The actual value to store in the database\r\nâ€¢ A human-readable name to represent the choice\r\nYouâ€™ve also specified a default value: the value associated with the Live status, which will\r\ndenote weblog entries to be displayed live on the site.\r\nYou can use choices with any of Djangoâ€™s model field types, but generally itâ€™s most useful\r\nwith IntegerField (where you can use it to provide meaningful names for a list of numeric\r\nchoices) and CharField (where, for example, you can use it to store short abbreviations in the\r\ndatabase, but still keep track of the full words or phrases they represent).\r\nIf youâ€™ve used other programming languages that support enumerations, this is a similar\r\nconcept. In fact, you could (and probably should) make it look a little bit more similar. Edit the\r\nEntry model so that it begins like this:\r\nclass Entry(models.Model):\r\nLIVE_STATUS = 1\r\nDRAFT_STATUS = 2\r\nSTATUS_CHOICES = (\r\n(LIVE_STATUS, ''Live''),\r\n(DRAFT_STATUS, ''Draft''),\r\n)', 1396501799, 10, '', '{"totalVote":0,"voterId":[]}', 0),
(8, 'java Tutorial 8', 'java-tut-8', '#java#basic#a#s#s#S#DD#javm m nb nbnvnvnvnvna#basic#a#s#s#S#DD#java#basic#a#s#s#S#DD#', 'java-basics', 'java', '2014-02-17', '<div class="well" id="python-block-1">def p():\n    w = int(''3'')\n    print w</div>', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0),
(9, 'My First Page', 'mui-h', '#python#basics#', 'python-basics', 'python', '2014-03-08', '<article>dbkhbcdb\r\nccdssd\r\ns\r\n\r\ns\r\ns\r\n\r\nss\r\n\r\ns\r\ns\r\ns\r\ns\r\ns\r\ns\r\ns\r\ns\r\nsd\r\n\r\nds\r\n\r\nq\r\n\r\nwq\r\nwq\r\nwq</article>', 1396501799, 0, '', '{"totalVote":0,"voterId":[]}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` int(2) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `type`) VALUES
(1, 'abhishekbanerjee1992@gmail.com', '3bf33de1c9c595dc14c4c0f41989bd3b45af4e0280fe062ef8350f88a91f503b16c53fefc07196bf34768b0a1167892e19c58ea7b19882333504616ac0e7294a', 'Abhishek', 2),
(2, 'abhishekbanerjee1992@ymail.com', '3bf33de1c9c595dc14c4c0f41989bd3b45af4e0280fe062ef8350f88a91f503b16c53fefc07196bf34768b0a1167892e19c58ea7b19882333504616ac0e7294a', 'Abhishek Banerjee', 2);

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `page_id` int(11) NOT NULL,
  `page` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `upvote` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `votes`
--

INSERT INTO `votes` (`page_id`, `page`, `user_id`, `upvote`) VALUES
(9, 'blog', 21, 1),
(9, 'blog', 25, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
